//
//  FrienjiApiTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Frienji

class FrienjiApiTests: XCTestCase {
    
    var api:FakeFrienjiApi!
    
    override func setUp() {
        super.setUp()
        
        self.api = FakeFrienjiApi()
    }
    
    override func tearDown() {
        super.tearDown()
        
        self.api = nil
    }
    
}