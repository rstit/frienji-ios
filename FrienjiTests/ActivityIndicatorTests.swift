//
//  ActivityIndicatorTests.swift
//  Frienji
//
//  Created by Piotr Łyczba on 14/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import XCTest
import RxSwift
import RxTests
import RxCocoa

@testable import Frienji

class ActivityIndicatorTests: XCTestCase {

    let inputValues = ["x": ()]
    let booleans = ["t": true, "f": false]

    func testTrackActivity() {
        // given
        let scheduler = TestScheduler(initialClock: 0)
        let activityIndicator = ActivityIndicator()

        let (firstInput, secondInput, expectedOutput) = (
            scheduler.parseEventsAndTimes("--x-|------", values: inputValues).first!,
            scheduler.parseEventsAndTimes("-----x-|---", values: inputValues).first!,

            scheduler.parseEventsAndTimes("t------f---", values: booleans).first!
        )

        // when
        let disposeBag = DisposeBag()
        scheduler.createHotObservable(firstInput).trackActivity(activityIndicator).subscribe().addDisposableTo(disposeBag)
        scheduler.createHotObservable(secondInput).trackActivity(activityIndicator).subscribe().addDisposableTo(disposeBag)
        let recordedOutput = scheduler.record(activityIndicator.asObservable())

        scheduler.start()

        // then
        XCTAssertEqual(recordedOutput.events, expectedOutput)
    }

}
