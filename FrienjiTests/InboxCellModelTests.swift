//
//  InboxCellModelTests.swift
//  Frienji
//
//  Created by Piotr Łyczba on 19/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import XCTest

@testable import Frienji

class InboxCellModelTests: XCTestCase {
    
    func testLoadCommand() {
        //given
        let viewModel = InboxCellModel(authorName: "name1", createdAt: "today", details: "details1", avatar: nil, isRead: false)
        
        let avatar = Avatar(name: "", type: .Emoji)
        let frienji = Frienji(userId: 1, username: "name2", phoneNumber: nil, createdAt: NSDate(), avatar: avatar, backgroundImageUrl: nil, lastLatitude: nil, lastLongitude: nil, whoAreYou: nil, relation: .None)
        let message = Message(id: 1, content: "details2", createdAt: NSDate(timeIntervalSinceNow: -2), isAuthor: true, attachmentImageUrl: nil, latitude: nil, longitude: nil)
        let conversation = Conversation(id: 1, lastMessage: message, receiver: frienji, unreadCount: 0)
        
        //when
        let result = viewModel.executeCommand(.Load(conversation: conversation))
        
        //then
        XCTAssertEqual("name2", result.authorName)
        XCTAssertEqual("details2", result.details)
        XCTAssertNotNil(result.avatar)
        XCTAssertTrue(result.isRead)
    }
    
    func testMarkReadCommand() {
        //given
        let viewModel = InboxCellModel(authorName: "", createdAt: "", details: "", avatar: nil, isRead: false)
        
        //when
        let result = viewModel.executeCommand(.MarkAsRead)
        
        //then
        XCTAssertTrue(result.isRead)
    }
    
}
