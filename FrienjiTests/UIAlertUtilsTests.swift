//
//  UIAlertUtilsTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Frienji

class UIAlertUtilsTests : XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        super.tearDown()
        
    }

    // MARK: Tests

    func testThatAlertIsShown() {
        
        //given
        let readyExpectation = expectationWithDescription("ready")
        
        let controller = UIViewController()
        UIApplication.sharedApplication().keyWindow?.rootViewController = controller
        XCTAssertNil(controller.presentedViewController)
        
        //when
        UIAlertUtils.showAlertWithTitle("title", fromController: controller) { () -> Void in
            
            XCTAssertNotNil(controller.presentedViewController, "Presented view controller should not be nil")
            readyExpectation.fulfill()
        }
        
        //then
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
}