//
//  PreviewViewModelTests.swift
//  Frienji
//
//  Created by Piotr Łyczba on 13/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import XCTest

@testable import Frienji

class PreviewViewModelTests: XCTestCase {

    func testCatchAndDismissOnKeepButtonTap() {
        // Given
        let input = ExplorationInput()
        let viewModel = createViewModel(input)
        viewModel.frienji.value = Frienji.fake()

        // Then
        let inputValues = ["v": ()]
        let outputValues = ["x": viewModel.frienji.value!]
        XCTAssertEvents(
            inputValues: inputValues, inputEvents: "-v-v-|", inputSubject: viewModel.catched,
            outputValues: outputValues, outputEvents: "-x-x--", outputObservable: input.catched
        )
    }

    func testRejectAndDismissOnDiscardButtonTap() {
        // Given
        let input = ExplorationInput()
        let viewModel = createViewModel(input)
        viewModel.frienji.value = Frienji.fake()

        // Then
        let inputValues = ["v": ()]
        let outputValues = ["x": viewModel.frienji.value!]
        XCTAssertEvents(
            inputValues: inputValues, inputEvents: "-v-v-|", inputSubject: viewModel.rejected,
            outputValues: outputValues, outputEvents: "-x-x--", outputObservable: input.rejected
        )
    }
    
    func createViewModel(input: Input? = nil) -> PreviewViewModel {
        return PreviewViewModel(navigationHandler: FakeNavigationHandler(input: input))
    }

}
