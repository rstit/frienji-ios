//
//  ShaderUtilsTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 19.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Frienji

class ShaderUtilsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()

    }
    
    override func tearDown() {
        super.tearDown()
        
    }
    
    // MARK: Tests

    func testThatSimpleShaderWillBeCompiled() {
        
        //given
        let shaderFileName = "TestVertexShader"
        
        //when
        let handle = ShaderUtils.compileShaderWithName(shaderFileName, shaderType: UInt32(GL_VERTEX_SHADER), bundle: NSBundle(forClass: ShaderUtilsTests.self))
        
        //then
        XCTAssertNotNil(handle, "Shader should be compiled")
    }
    
}