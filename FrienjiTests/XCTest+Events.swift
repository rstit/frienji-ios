//
//  XCTAssert+Events.swift
//  Frienji
//
//  Created by Piotr Łyczba on 13/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxTests
import RxSwift

func XCTAssertEvents<InputElement, OutputElement: Equatable>(
    inputValues inputValues: [String: InputElement], inputEvents: String, inputSubject: PublishSubject<InputElement>,
    outputValues: [String: OutputElement], outputEvents: String, outputObservable: Observable<OutputElement>
    ) {
    let scheduler = TestScheduler(initialClock: 0)

    let input = scheduler.parseEventsAndTimes(inputEvents, values: inputValues).first!
    let output = scheduler.parseEventsAndTimes(outputEvents, values: outputValues).first!

    let disposeBag = DisposeBag()
    scheduler.createHotObservable(input).asObservable().bindTo(inputSubject).addDisposableTo(disposeBag)
    let recordedLoads = scheduler.record(outputObservable)

    scheduler.start()

    XCTAssertEqual(recordedLoads.events, output)
}
