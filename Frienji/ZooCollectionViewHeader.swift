//
//  ZooCollectionViewHeader.swift
//  Frienji
//
//  Created by Piotr Łyczba on 07/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class ZooCollectionViewHeader: UICollectionReusableView {
    
    @IBOutlet weak var headerTitle: UILabel!
    
}
