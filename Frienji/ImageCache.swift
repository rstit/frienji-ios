//
//  ImageCache.swift
//  Frienji
//
//  Created by Piotr Łyczba on 03/11/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import RxSwift
import CoreLocation

protocol ImageCache {

    static var sharedInstance: ImageCache { get }

    func imageForKey(key: String) -> UIImage?

    func storeImageForKey(image: UIImage?, forKey key: String)

}

class ImageCacheSDWebInMemory: ImageCache {

    private let cache = SDImageCache.sharedImageCache()

    static var sharedInstance: ImageCache = ImageCacheSDWebInMemory()

    func imageForKey(key: String) -> UIImage? {
        return cache.imageFromMemoryCacheForKey(key)
    }

    func storeImageForKey(image: UIImage?, forKey key: String) {
        cache.storeImage(image, forKey: key)
    }

}

extension ObservableConvertibleType where E == UIImage? {

    func cachedImageForKey(key: String) -> Observable<UIImage?> {
        let cache = ImageCacheSDWebInMemory.sharedInstance

        return cache
            .imageForKey(key)
            .map(Observable.just)
            ?? asObservable()
                .doOnNext {
                    cache.storeImageForKey($0, forKey: key)
                }
    }

    func cachedImageForLocation(location: CLLocation) -> Observable<UIImage?> {
        return cachedImageForKey("location:\(location.coordinate.latitude),\(location.coordinate.longitude)")
    }

}
