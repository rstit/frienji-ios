//
//  PostView.swift
//  Frienji
//
//  Created by Piotr Łyczba on 06/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

@IBDesignable class PostView: UIView, NibInstantiate {

    // MARK: - Outlets

    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var imageAttachment: UIImageView!
    @IBOutlet weak var locationAttachment: UIImageView!

    let loaded = PublishSubject<Post>()
    let disposeBag = DisposeBag()

    // MARK: - Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    private func loadViewFromNib() {
        let view = self.instantiateFromNib()
        self.addSubviewFullscreen(view)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        #if !TARGET_INTERFACE_BUILDER
        let addToDisposeBag = { (disposable: Disposable) in disposable.addDisposableTo(self.disposeBag) }

        // View model
        let viewModel = PostViewModel.viewModel(
            postLoaded: loaded
        )

        // Bind view model
        [
            viewModel.map { $0.authorName } --> authorName.rx_text,
            viewModel.map { $0.createdAt } --> createdAt.rx_text,
            viewModel.map { $0.details } --> details.rx_text,
            viewModel.map { $0.avatar } --> avatar.rx_image,
            viewModel.map { $0.imageUrl } --> imageAttachment.sd_setImageWithURL,
        ].forEach(addToDisposeBag)
        viewModel
            .map { $0.location }
            .distinctUntilChanged(==)
            .flatMap { $0?.mapSnapshot.map(Optional.init) ?? Observable.just(nil) }
            .subscribeNext { [unowned self] image in
                self.locationAttachment.image = image
            }
            .addDisposableTo(disposeBag)
        #endif
    }
    
}
