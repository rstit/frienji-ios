//
//  AvatarSelectionViewModel.swift
//  Frienji
//
//  Created by Adam Szeremeta on 29.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AvatarSelectionViewModel {
    
    private let kNumberOfAnimatedImages = 10
    private let kNumberOfImages = 9
    
    private let kImagePrefix = "image"
    private let kImageExtension = "png"
    
    private let kAnimatedImagePrefix = "avatar"
    private let kAnimatedImageExtension = "gif"
    
    private let emojis = String.emojisArray
    
    // MARK: Init
    
    init() {

    }
    
    // MARK: Data
    
    func getNumberOfSections() -> Int {
        return AvatarType.allValues.count
    }
    
    func getNumberOfItemsForSection(section:Int) -> Int {
        //familiars, weird creatures, emoji
        let typeForSection = AvatarType.allValues.reverse()[section]
        
        switch typeForSection {
            
        case .AnimatedImage:
            return self.kNumberOfAnimatedImages
            
        case .Image:
            return self.kNumberOfImages
            
        case .Emoji:
            return self.emojis.count
        }
    }
    
    func getTitleForSection(section:Int) -> String? {
        let typeForSection = AvatarType.allValues.reverse()[section]
        return AvatarType.labels[typeForSection]
    }
    
    func getItemForIndexPath(indexPath:NSIndexPath) -> (value:String, type:AvatarType) {
        let typeForSection = AvatarType.allValues.reverse()[indexPath.section]

        switch typeForSection {
            
        case .AnimatedImage:
            return ("\(self.kAnimatedImagePrefix)\(indexPath.row + 1).\(self.kAnimatedImageExtension)", typeForSection)
            
        case .Image:
            return ("\(self.kImagePrefix)\(indexPath.row + 1).\(self.kImageExtension)", typeForSection)
            
        case .Emoji:
            return (self.emojis[indexPath.row], typeForSection)
        }
    }
    
    // MARK: Avatar info
    
    func storeAvatarInfo(avatarName:String, skinTone:String?, avatarType:AvatarType) {
        Settings.sharedInstance.userAvatarName = avatarType == .Emoji ? avatarName.emojiEscaped : avatarName
        Settings.sharedInstance.userAvatarGroup = avatarType
        Settings.sharedInstance.userAvatarSkinTone = skinTone
    }
}
