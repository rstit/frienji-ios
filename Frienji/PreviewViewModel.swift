//
//  PreviewViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 12/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift

class PreviewViewModel {

    let frienji = Variable<Frienji?>(nil)

    let catched = PublishSubject<Void>()
    let rejected = PublishSubject<Void>()

    let disposeBag = DisposeBag()

    // MARK: Init
    
    init(navigationHandler: NavigationHandler) {
        catched.flatMap { [unowned self] in
            self.frienji.asObservable()
        }.bindTo(navigationHandler.modalDismissed(ExplorationViewController.self)) { input, frienji in
            if var frienji = frienji {
                frienji.relation = .Catched
                input.catched.onNext(frienji)
            }
        }.addDisposableTo(disposeBag)
        
        rejected.flatMap { [unowned self] in
            self.frienji.asObservable()
        }.bindTo(navigationHandler.modalDismissed(ExplorationViewController.self)) { input, frienji in
            if var frienji = frienji {
                frienji.relation = .Rejected
                input.rejected.onNext(frienji)
            }
        }.addDisposableTo(disposeBag)
    }

}
