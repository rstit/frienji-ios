//
//  BubblePopupAnimator.swift
//  Traces
//
//  Created by Adam Szeremeta on 28.10.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class BubblePopupAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var startingFrame = CGRectZero
    
    // MARK: UIViewControllerAnimatedTransitioning
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        guard let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) else {
            return
        }
        
        let containerView = transitionContext.containerView()
        let toView = toViewController.view
        
        let toViewStartFrame = self.startingFrame
        let toViewFinalFrame = transitionContext.finalFrameForViewController(toViewController)
        
        //scale
        let xScaleFactor = toViewStartFrame.width / toViewFinalFrame.width
        let yScaleFactor = toViewStartFrame.height / toViewFinalFrame.height
        let scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor)
        
        toView.transform = scaleTransform
        toView.center = CGPointMake(CGRectGetMidX(toViewStartFrame), toViewStartFrame.origin.y + toViewStartFrame.size.height / 2)
        toView.alpha = 0
        
        containerView.addSubview(toView)
        
        UIView.animateWithDuration(self.transitionDuration(transitionContext), animations: { () -> Void in
            toView.transform = CGAffineTransformIdentity
            toView.center = CGPointMake(toViewFinalFrame.size.width / 2, toViewFinalFrame.size.height / 2)
            toView.alpha = 1
            
        }) { (result:Bool) -> Void in
            let success = !transitionContext.transitionWasCancelled()
            transitionContext.completeTransition(success)
        }
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return kDefaultAnimationDuration
    }
    
}

extension BubblePopupAnimator: UIViewControllerTransitioningDelegate {
    
    // MARK: UIViewControllerTransitioningDelegate protocol methods
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
    
    func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
    
}
