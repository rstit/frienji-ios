//
//  WallViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import Curry
import CoreLocation

enum WallCommand {

    case LoadFrienji(frienji: Frienji)
    case LoadPosts(posts: [Post])
    case SendPost(post: Post)
    case SendComment(post: Post)
    case LikePost(post: Post)
    case DislikePost(post: Post)
    case ReportPost(post: Post)
}

struct WallViewModel {

    let frienjiName: String
    let posts: [Post]
    
    let disposeBag = DisposeBag()

    init(frienjiName: String = "", posts: [Post] = []) {
        self.frienjiName = frienjiName
        self.posts = posts
    }

    func executeCommand(command: WallCommand) -> WallViewModel {
        switch command {
        case let .LoadFrienji(frienji):
            return WallViewModel(frienjiName: frienji.username, posts: posts)
            
        case let .LoadPosts(posts):
            return WallViewModel(frienjiName: frienjiName, posts: posts)
            
        case let .SendPost(post):
            return WallViewModel(frienjiName: frienjiName, posts: posts.arrayByInserting(post, atIndex: 0))
            
        case let .SendComment(post):
            return WallViewModel(frienjiName: frienjiName, posts: posts.arrayByAppending(post))
            
        case let .LikePost(post):
            let posts: [Post] = self.posts.map {
                if $0 == post {
                    return Post(id: $0.id, message: $0.message, createdAt: $0.createdAt, isAuthor: $0.isAuthor, numberOfComments: $0.numberOfComments, numberOfLikes: $0.numberOfLikes + 1, liked: true, author: $0.author, attachmentUrl: $0.attachmentUrl, latitude: $0.latitude, longitude: $0.longitude, wallOwnerId: $0.wallOwnerId, parentPostId: $0.parentPostId, avatarTrail: $0.avatarTrail, reported: $0.reported)
                } else {
                    return $0
                }
            }
            
            let viewModel = WallViewModel(frienjiName: frienjiName, posts: posts)
            FrienjiApi.sharedInstance.likePost(post).subscribe().addDisposableTo(viewModel.disposeBag)
            
            return viewModel
            
        case let .DislikePost(post):
            let posts: [Post] = self.posts.map {
                if $0 == post {
                    return Post(id: $0.id, message: $0.message, createdAt: $0.createdAt, isAuthor: $0.isAuthor, numberOfComments: $0.numberOfComments, numberOfLikes: $0.numberOfLikes - 1, liked: false, author: $0.author, attachmentUrl: $0.attachmentUrl, latitude: $0.latitude, longitude: $0.longitude, wallOwnerId: $0.wallOwnerId, parentPostId: $0.parentPostId, avatarTrail: $0.avatarTrail, reported: $0.reported)
                } else {
                    return $0
                }
            }
            
            let viewModel = WallViewModel(frienjiName: frienjiName, posts: posts)
            FrienjiApi.sharedInstance.likePost(post).subscribe().addDisposableTo(viewModel.disposeBag)
            
            return viewModel
            
        case let .ReportPost(post):
            let posts: [Post] = self.posts.map {
                if $0 == post {
                    return Post(id: $0.id, message: $0.message, createdAt: $0.createdAt, isAuthor: $0.isAuthor, numberOfComments: $0.numberOfComments, numberOfLikes: $0.numberOfLikes, liked: $0.liked, author: $0.author, attachmentUrl: $0.attachmentUrl, latitude: $0.latitude, longitude: $0.longitude, wallOwnerId: $0.wallOwnerId, parentPostId: $0.parentPostId, avatarTrail: $0.avatarTrail, reported: true)
                } else {
                    return $0
                }
            }
            
            let viewModel = WallViewModel(frienjiName: frienjiName, posts: posts)
            FrienjiApi.sharedInstance.reportPost(post).subscribe().addDisposableTo(viewModel.disposeBag)
            
            return viewModel
        }
    }

}

extension WallViewModel {

    // MARK: - Create observable

    static func create(withProfileLoaded profile: Observable<Frienji>, textSent text: Observable<String>, imageAttached image: BehaviorSubject<UIImage?>, locationAttached location: BehaviorSubject<CLLocation?>, loadMoreTriggered loadMore: Observable<Void>, liked like: Observable<Post>, disliked dislike: Observable<Post>, reported report: Observable<Post>, viewAppeared appeared: Observable<Void>) -> Observable<WallViewModel> {
        return create(withCommands: loadProfileCommand(profile), loadPostsCommand(profile, loadMore: loadMore), sendPostCommand(profile, text: text, image: image, location: location), likePostCommand(like), dislikePostCommand(dislike), reportPostCommand(report), appearedReloadPostsCommand(profile, trigger: appeared, loadMore: loadMore))
    }
    
    static func create(withProfileLoaded profile: Observable<Frienji>, textSent text: Observable<String>, imageAttached image: BehaviorSubject<UIImage?>, locationAttached location: BehaviorSubject<CLLocation?>, loadMoreTriggered loadMore: Observable<Void>, liked like: Observable<Post>, disliked dislike: Observable<Post>,reported report: Observable<Post>) -> Observable<WallViewModel> {
        return create(withCommands: loadProfileCommand(profile), loadPostsCommand(profile, loadMore: loadMore), sendPostCommand(profile, text: text, image: image, location: location), likePostCommand(like), dislikePostCommand(dislike), reportPostCommand(report))
    }

    static func create(withProfileLoaded profile: Observable<Frienji>, postLoaded post: Observable<Post>, textSent text: Observable<String>, imageAttached image: BehaviorSubject<UIImage?>, locationAttached location: BehaviorSubject<CLLocation?>, loadMoreTriggered loadMore: Observable<Void>, liked like: Observable<Post>, disliked dislike: Observable<Post>, reported report: Observable<Post>) -> Observable<WallViewModel> {
        return create(withCommands: loadProfileCommand(profile), loadPostsCommand(profile, post: post, loadMore: loadMore), sendCommentCommand(profile, post: post, text: text, image: image, location: location), likePostCommand(like), dislikePostCommand(dislike), reportPostCommand(report))
    }

    static func create(withCommands commands: Observable<WallCommand>...) -> Observable<WallViewModel> {
        return commands.toObservable().merge()
            .scan(WallViewModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .startWith(WallViewModel())
            .shareReplay(1)
    }

    // MARK: - Commands

    private static func loadProfileCommand(profile: Observable<Frienji>) -> Observable<WallCommand> {
        return profile.map(WallCommand.LoadFrienji)
    }

    private static func loadPostsCommand(profile: Observable<Frienji>, loadMore: Observable<Void>) -> Observable<WallCommand> {
        return profile
            .flatMap { FrienjiApi.sharedInstance.getFrienjisPosts($0, loadMore: loadMore) }
            .map(WallCommand.LoadPosts)
    }

    private static func loadPostsCommand(profile: Observable<Frienji>, post: Observable<Post>, loadMore: Observable<Void>) -> Observable<WallCommand> {
        return Observable.combineLatest(profile, post) { ($0, $1) }
            .flatMap { FrienjiApi.sharedInstance.getPostComments($0, post: $1, loadMore: loadMore) }
            .map(WallCommand.LoadPosts)
    }

    private static func sendPostCommand(profile: Observable<Frienji>, text: Observable<String>, image: BehaviorSubject<UIImage?>, location: BehaviorSubject<CLLocation?>) -> Observable<WallCommand> {
        return Observable.combineLatest(profile, text) { curry(FrienjiApi.sharedInstance.sendPost)($0)($1) }
            .withLatestFrom(image) { sendPost, image in
                sendPost(image)
            }
            .withLatestFrom(location) { sendPost, location in
                sendPost(location?.coordinate.latitude)(location?.coordinate.longitude)
            }
            .flatMap { $0 }
            .doOnNext { [weak image, weak location] _ in
                image?.onNext(nil)
                location?.onNext(nil)
            }
            .map(WallCommand.SendPost)
    }

    private static func sendCommentCommand(profile: Observable<Frienji>, post: Observable<Post>, text: Observable<String>, image: BehaviorSubject<UIImage?>, location: BehaviorSubject<CLLocation?>) -> Observable<WallCommand> {
        return Observable.combineLatest(profile, post, text) { curry(FrienjiApi.sharedInstance.sendComment)($0)($1)($2) }
            .withLatestFrom(image) { sendPost, image in
                sendPost(image)
            }
            .withLatestFrom(location) { sendPost, location in
                sendPost(location?.coordinate.latitude)(location?.coordinate.longitude)
            }
            .flatMap { $0 }
            .doOnNext { [weak image, weak location] _ in
                image?.onNext(nil)
                location?.onNext(nil)
            }
            .map(WallCommand.SendComment)
    }
    
    private static func likePostCommand(like: Observable<Post>) -> Observable<WallCommand> {
        return like.map(WallCommand.LikePost)
    }
    
    private static func dislikePostCommand(dislike: Observable<Post>) -> Observable<WallCommand> {
        return dislike.map(WallCommand.DislikePost)
    }
    
    private static func reportPostCommand(report: Observable<Post>) -> Observable<WallCommand> {
        return report.map(WallCommand.ReportPost)
    }
    
    private static func appearedReloadPostsCommand(profile: Observable<Frienji>, trigger: Observable<Void>, loadMore: Observable<Void>) -> Observable<WallCommand> {
        return Observable.create({ (subscriber) -> Disposable in
            
            var disposeBag = DisposeBag()
            
            let cancel = AnonymousDisposable.init({
                disposeBag = DisposeBag()
            })
            
            trigger.bindNext({
                profile.flatMap { FrienjiApi.sharedInstance.getFrienjisPosts($0, loadMore: loadMore) }.map { posts in posts.filter { $0.avatarTrail == nil }}.bindNext({ (posts:[Post]) in
                    subscriber.onNext(WallCommand.LoadPosts(posts: posts))
                }).addDisposableTo(disposeBag)
            }).addDisposableTo(disposeBag)
            
            return cancel
        })
    }

}
