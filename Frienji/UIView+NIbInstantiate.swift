//
//  UIView+NibInstantiate.swift
//  Frienji
//
//  Created by adam kolodziej on 09.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

protocol NibInstantiate {
}

extension NibInstantiate where Self: UIView {
    func instantiateFromNib() -> UIView {
        let bundle = NSBundle(forClass: Self.self)
        let nib = UINib(nibName: String(Self), bundle: bundle)
        return nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    }
}
