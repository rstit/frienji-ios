//
//  BaseViewController.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController : UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var scrollView:UIScrollView!
    
    // MARK:-  Life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.registerForKeyboardNotifications()
        self.addTapGestureToView()
    }
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    // MARK: - Keyboard Notification
    
    private func registerForKeyboardNotifications()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BaseViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BaseViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BaseViewController.keyboardWillChangeSize(_:)), name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    // MARK: - Keyboard Handling

    func keyboardWillShow(notification: NSNotification)
    {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let animationTime = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
        
        keyboardWillShowWithSize(keyboardSize, animationTime: animationTime ?? 0.33)
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        self.keyboardWillHide()
    }
    
    func keyboardWillChangeSize(notification:NSNotification) {
        
        guard let size = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue().size else {
            return
        }
        
        keyboardWillChangeSize(newSize: size)
    }
    
    // MARK: - Keyboard Handling
    
    func keyboardWillShowWithSize(keyboardSize: CGSize?, animationTime:Double) {
        
        guard let _ = self.scrollView else {
            return
        }
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardSize!.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide() {
        
        guard let _ = self.scrollView else {
            return
        }

        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillChangeSize(newSize size:CGSize) {
        
        //implement in controller
    }
    

    // MARK: - Tap Gesture
    
    private func addTapGestureToView()
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        
        self.view.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - UIGestureRecognizerDelegate
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool
    {
        return !(touch.view is UIButton || touch.view is UITextField)
    }
    
    // MARK: - Keyboard dismiss
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
}
