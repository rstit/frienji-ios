//
//  ChatViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 20/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import CoreLocation

class ChatViewController: BaseMessageViewController {
    
    //outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var deleteView: DeleteView!
    @IBOutlet weak var cancelSelectionButton: UIBarButtonItem!
    
    //properties
    private var viewModel: ChatViewModel!
    
    private let notificationHandler = NotificationHandler()
    private var transitionManager:ImageTransitionManager?
    
    private var userInteractionPerformed = false
    
    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewModel = ChatViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        setUpBindings()
        
        self.deleteView.delegate = self
        
        if let title = self.viewModel.conversation.value?.receiver.username {
            self.navigationItem.title = title
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        LocationManager.sharedInstance.startLocationUpdates()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.Chat) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.Chat) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.Chat)
            }
            coachmark.showWithAnimationInContainer()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        LocationManager.sharedInstance.stopLocationUpdates()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let navigationController = segue.destinationViewController as? UINavigationController else {
            return
        }
        
        if let viewController = navigationController.viewControllers.first as? LocationPickerViewController {
            navigationController.popoverPresentationController?.delegate = self
            viewController.delegate = self
        } else if let viewController = navigationController.viewControllers.first as? SendPhotoViewController {
            viewController.delegate = self
            viewController.photo = sender as? UIImage ?? nil
            navigationController.popoverPresentationController?.delegate = self
        }
    }
    
    // MARK: Actions
    
    @IBAction func onCancelSelectionButtonTouch(sender: AnyObject) {
        self.viewModel.deselectAllMessages()
        self.viewModel.selectionModeEnabled.value = false
        
        self.deleteView.hidden = true
    }
    
    // MARK: Data
    
    func setConversation(conversation:Conversation?) {
        self.viewModel.conversation.value = conversation
        
        //check if view is loaded
        if self.isViewLoaded() {
            setUpBindings()
            
            if let title = conversation?.receiver.username {
                self.navigationItem.title = title
            }
        }
    }
    
    // MARK: TableView
    
    private func configureTableView() {
        let cellNib = UINib(nibName: ChatViewCell.reuseIdentifier, bundle: NSBundle(forClass: ChatViewCell.self))
        self.tableView.registerNib(cellNib, forCellReuseIdentifier: ChatViewCell.reuseIdentifier)
        
        let avatarChangeNib = UINib(nibName: AvatarChangeCell.reuseIdentifier, bundle: NSBundle(forClass: AvatarChangeCell.self))
        self.tableView.registerNib(avatarChangeNib, forCellReuseIdentifier: AvatarChangeCell.reuseIdentifier)
        
        let headerNib = UINib(nibName: TimeHeaderView.reuseIdentifier, bundle: NSBundle(forClass: TimeHeaderView.self))
        self.tableView.registerNib(headerNib, forHeaderFooterViewReuseIdentifier: TimeHeaderView.reuseIdentifier)
        
        self.tableView.estimatedSectionHeaderHeight = TimeHeaderView.kEstimatediewHeight
        self.tableView.estimatedRowHeight = ChatViewCell.kEstimatedViewHeight
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    // MARK: Bindings
    
    private func setUpBindings() {
        self.disposeBag = DisposeBag()
        
        //messages
        self.viewModel.messages.asObservable().bindNext { [unowned self] (messages:[NSDate : [ChatData]]) in
            if self.viewModel.shouldScrollToBottom.value {
                //scroll to bottom in a way that user already see last message
                self.tableView.reloadData()
                self.tableView.layoutIfNeeded()
                
                let section = self.viewModel.messages.value.keys.count - 1
                if section >= 0 {
                    
                    guard let sectionDate = self.viewModel.getSectionItemForIndex(section), let count = self.viewModel.messages.value[sectionDate]?.count else {
                        return
                    }
                    
                    let indexPath = NSIndexPath(forRow: count - 1, inSection: section)
                    self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: false)
                }
            } else {
                //we want to maintain position in table after reload (rows added on top)
                let currentContentHeight = self.tableView.contentSize.height
                
                self.tableView.reloadData()
                self.tableView.layoutIfNeeded()
                
                let newContentOffset = self.tableView.contentOffset.y
                let newHeight = self.tableView.contentSize.height

                self.tableView.setContentOffset(CGPointMake(0, newHeight - currentContentHeight + newContentOffset), animated: false)
                
                self.viewModel.isFetchingNextPage.value = false
            }
        }.addDisposableTo(self.disposeBag)
        
        //selection mode
        self.viewModel.selectionModeEnabled.asObservable().bindNext { [unowned self] enabled in
            self.tableView.allowsMultipleSelection = enabled
            self.cancelSelectionButton.tintColor = enabled ? UIColor.blackColor() : UIColor.clearColor()
            
            self.tableView.reloadData()
        }.addDisposableTo(self.disposeBag)
        
        
        //indicator
        Observable.combineLatest(self.viewModel.isInitialFetchInProgress.asObservable(), self.viewModel.sendingInProgress.asObservable()) { fetchingProgress, sendingProgrres in
            fetchingProgress || sendingProgrres
        }.map { !$0 }.bindTo(self.loadingIndicator.rx_hidden).addDisposableTo(self.disposeBag)
        
        //push
        if let receiver = self.viewModel.conversation.value?.receiver {
            self.notificationHandler.messageByAuthor(receiver).showAlertOnApiError(self).bindNext({ [weak self] (message:Message) in
                self?.viewModel.incommingMessage.value = message
            }).addDisposableTo(self.disposeBag)
            
            self.notificationHandler.deletedMessageByAuthor(receiver).bindNext({ [weak self] (message:Message) in
                self?.viewModel.deletedMessage.value = message
            }).addDisposableTo(self.disposeBag)
        }
        
        // Load more trigger
        let contentOffset: Observable<CGPoint?> = self.tableView.rx_observeWeakly(CGPoint.self, "contentOffset", options: [.New])
        contentOffset.asObservable().bindNext { [weak self] (offset:CGPoint?) in
            
            guard let strongSelf = self else {
                return
            }
            
            let isCurrentlyFetching = strongSelf.viewModel.isInitialFetchInProgress.value || strongSelf.viewModel.isFetchingNextPage.value
            let offsetMargin = ChatViewCell.kEstimatedViewHeight * 3
            
            if !isCurrentlyFetching && offset?.y < offsetMargin && strongSelf.userInteractionPerformed {
                strongSelf.viewModel.isFetchingNextPage.value = true
                strongSelf.viewModel.loadMoreTrigger.onNext()
            }
        }.addDisposableTo(self.disposeBag)
    }
    
    // MARK: - LocationPickerViewControllerDelegate
    
    override func locationPicker(picker: LocationPickerViewController, didFinishPickingLocation location: CLLocationCoordinate2D, withMessage message:String) {
        self.viewModel.postMessageWithArguments((message, nil, location), fromController: self)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - SendPhotoViewControllerDelegate
    
    override func sendPhotoController(controller: SendPhotoViewController, didFinishPreparingPhoto photo: UIImage, withMessage message: String) {
        self.viewModel.postMessageWithArguments((message, photo, nil), fromController: self)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - MessageToolbarProtocol
    
    override func didTouchSendWithMesssage(message: String, forMessageToolBarView messageToolBarView: MessageToolbarView) {
        self.viewModel.postMessageWithArguments((message, nil, nil), fromController: self)
    }
    
}

extension ChatViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.viewModel.messages.value.keys.count
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterViewWithIdentifier(TimeHeaderView.reuseIdentifier) as! TimeHeaderView
        let sectionDate = self.viewModel.getSectionDisplayDateForIndex(section)
        
        view.configureWithDate(sectionDate)
        
        return view
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let sectionDate = self.viewModel.getSectionItemForIndex(section), let count = self.viewModel.messages.value[sectionDate]?.count else {
            return 0
        }
        
        return count
    }
        
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let item = self.viewModel.getRowItemForIndexPath(indexPath) else {
            return tableView.dequeueReusableCellWithIdentifier(ChatViewCell.reuseIdentifier, forIndexPath: indexPath)
        }
        
        switch item {
            
        case .UserMessage(let message):
            let cell = tableView.dequeueReusableCellWithIdentifier(ChatViewCell.reuseIdentifier, forIndexPath: indexPath) as! ChatViewCell
            let user = self.viewModel.getUserForMessage(message)
           
            cell.configureWithMessage(message, andUser: user)
            cell.delegate = self
            cell.setSelectionModeEnabled(self.viewModel.selectionModeEnabled.value && message.isAuthor)
            
            if self.viewModel.selectedMessages.contains(message) {
                tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
            }
                        
            return cell
            
        case .UserAvatarChange(let avatarChange):
            let cell = tableView.dequeueReusableCellWithIdentifier(AvatarChangeCell.reuseIdentifier, forIndexPath: indexPath) as! AvatarChangeCell
            cell.configureWithAvatarChange(avatarChange)
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.viewModel.selectMessageAtIndexPath(indexPath)
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        self.viewModel.deselectMessageAtIndexPath(indexPath)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.userInteractionPerformed = true
    }
}

extension ChatViewController: ChatViewCellProtocol {
    
    func chatViewCellDidLongPress(cell: ChatViewCell) {
        UIActionSheetUtils.showMessageActionsSheet(fromController: self) { [unowned self] (selectedAction) in
            if selectedAction == .DeleteMessages {
                self.viewModel.selectionModeEnabled.value = true
                self.deleteView.hidden = false
            }
        }
    }
    
    func chatViewCellDidTouchImageAttachment(cell: ChatViewCell, imageView: UIImageView) {
        self.showImageFullscreen(cell, imageView: imageView)
    }
    
    func chatViewCellDidTouchLocationAttachment(cell: ChatViewCell, imageView: UIImageView) {
        
        guard let indexPath = self.tableView.indexPathForCell(cell), let item = self.viewModel.getRowItemForIndexPath(indexPath) else {
            return
        }
        
        if case .UserMessage(let message) = item, let latitude = message.latitude, let longitude = message.longitude {
            self.showLocationFullscreen(cell, imageView: imageView, location: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        }
    }
    
    private func showImageFullscreen(cell:UITableViewCell, imageView:UIImageView) {
        
        guard let image = imageView.image else {
            return
        }
        
        self.transitionManager = ImageTransitionManager()
        self.transitionManager?.startingFrame = cell.convertRect(imageView.frame, toView: self.view)
        self.transitionManager?.presentingController = self
        
        let controller = FullScreenImageController.loadFromStoryboard()
        controller.image = image
        controller.modalPresentationStyle = .Custom
        controller.transitioningDelegate = self.transitionManager
        
        self.transitionManager?.modalController = controller
        
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    private func showLocationFullscreen(cell:UITableViewCell, imageView:UIImageView, location:CLLocationCoordinate2D) {
        self.transitionManager = ImageTransitionManager()
        self.transitionManager?.startingFrame = cell.convertRect(imageView.frame, toView: self.view)
        self.transitionManager?.presentingController = self
        
        let controller = FullScreenMapController.loadFromStoryboard()
        controller.pinLocation = location
        controller.modalPresentationStyle = .Custom
        controller.transitioningDelegate = self.transitionManager
        
        self.transitionManager?.modalController = controller
        
        self.presentViewController(controller, animated: true, completion: nil)
    }
}

extension ChatViewController: DeleteViewProtocol {
    
    func deleteViewDidTouchDelete() {
        
        guard self.viewModel.selectedMessages.count > 0 else {
            return
        }
        
        UIActionSheetUtils.showMessagesDeleteConfirmation(fromController: self, messagesCount: self.viewModel.selectedMessages.count) { [unowned self] in
            self.viewModel.deleteSelectedMessages(self)
            self.viewModel.selectionModeEnabled.value = false
            
            self.deleteView.hidden = true
        }
    }
}
