//
//  ZooCollectionViewCell.swift
//  Frienji
//
//  Created by bolek on 17.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

protocol ZooCollectionViewCellProtocol: class {
    
    func zooCollectionViewCellDidTouchDeleteButton(cell:ZooCollectionViewCell) -> Void
}

class ZooCollectionViewCell: UICollectionViewCell, CellReuseIdentifier {
    
    //outlets
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    weak var delegate: ZooCollectionViewCellProtocol?
    
    // MARK: Life cycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.cover.sd_cancelCurrentImageLoad()
        self.cover.image = nil
    }
    
    // MARK: - Data
    
    func confireCellForFrienji(frienji: Frienji, isViewInEditionMode editing: Bool) {
        userName.text = frienji.username
        avatar.image = frienji.avatar.avatarImage
        
        if let backgroundImageUrl = frienji.backgroundImageUrl {
            cover.sd_setImageWithURL(NSURL(string: backgroundImageUrl))
        }
        
        deleteButton.hidden = !editing
    }
    
    @IBAction func onDeleteButtonTouch(sender: UIButton) {
        self.delegate?.zooCollectionViewCellDidTouchDeleteButton(self)
    }
   
}
