//
//  UIActionSheetUtils.swift
//  Frienji
//
//  Created by Adam Szeremeta on 22.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class UIActionSheetUtils {
    
    enum MediaAction: Int {
        
        case Photos
        case Camera
    }
    
    enum MessageAction: Int {
        
        case DeleteMessages
        
        static let allValues = [DeleteMessages]
        
        var title: String {
            switch self {
                
            case .DeleteMessages:
                return Localizations.chat.actions.delete_messages
            }
        }
    }
    
    // MARK: Media picking
    
    class func showMediaPickingActionSheet(fromController controller:UIViewController, actionCallback:((selectedAction:MediaAction) -> Void)?, showCompletion:(() -> Void)?) {
        
        let actionSheetController = UIAlertController(title: Localizations.toolbar.action_sheet.title, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        //actions
        let photosAction = UIAlertAction(title: Localizations.toolbar.action_sheet.from_photos, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            actionCallback?(selectedAction: MediaAction.Photos)
        }
        
        let cameraAction = UIAlertAction(title: Localizations.toolbar.action_sheet.from_camera, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            actionCallback?(selectedAction: MediaAction.Camera)
        }
        
        let cancelAction = UIAlertAction(title: Localizations.toolbar.action_sheet.cancel, style: UIAlertActionStyle.Cancel) { (action:UIAlertAction) -> Void in
            
        }
        
        actionSheetController.addAction(photosAction)
        actionSheetController.addAction(cameraAction)
        actionSheetController.addAction(cancelAction)
        
        controller.presentViewController(actionSheetController, animated: true, completion: showCompletion)
    }
    
    // MARK: Message actions
    
    class func showMessageActionsSheet(fromController controller:UIViewController, actionCallback:((selectedAction:MessageAction) -> Void)?) {
        let actionSheetController = UIAlertController(title: Localizations.chat.actions.action_sheet_title, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)

        for messageAction in MessageAction.allValues {
            let alertAction = UIAlertAction(title: messageAction.title, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
                actionCallback?(selectedAction: messageAction)
            }

            actionSheetController.addAction(alertAction)
        }
        
        actionSheetController.addAction(UIAlertAction(title: Localizations.chat.actions.action_sheet_cancel, style: .Cancel, handler: nil))
        
        controller.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    class func showMessagesDeleteConfirmation(fromController controller:UIViewController, messagesCount:Int, actionCallback:() -> Void) {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let deleteAction = UIAlertAction(title: Localizations.chat.actions.delete_x_messages(messagesCount), style: UIAlertActionStyle.Destructive) { (action:UIAlertAction) -> Void in
            actionCallback()
        }

        actionSheetController.addAction(deleteAction)
        actionSheetController.addAction(UIAlertAction(title: Localizations.chat.actions.action_sheet_cancel, style: .Cancel, handler: nil))
        
        controller.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
}
