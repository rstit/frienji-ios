//
//  CatchViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 06/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

struct PreviewInput: Input {
    let loaded = ReplaySubject<Frienji>.create(bufferSize: 1)
}

class PreviewViewController: UIViewController, HasInput, StoryboardLoad {

    static var storyboardId: String = "Main"
    static var storyboardControllerId = "PreviewViewController"
    // MARK: - Outlets

    @IBOutlet weak var profile: ProfileView!
    @IBOutlet weak var discardButton: UIButton!
    @IBOutlet weak var keepButton: UIButton!
    
    @IBOutlet weak var keepLabel: UILabel!
    @IBOutlet weak var discardLabel: UILabel!

    // MARK: - Observables

    var input = PreviewInput()

    private let disposeBag = DisposeBag()

    // MARK: - Properties

    private var viewModel: PreviewViewModel!

    // MARK: - View's lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = PreviewViewModel(navigationHandler: NavigationHandler(sourceViewController: self))
        
        addPanGestureToFrienjiCard()
        setUpBindings()
    }
    
    // MARK: Bindings
    
    private func setUpBindings() {
        input.loaded.bindTo(profile.profileLoaded).addDisposableTo(disposeBag)
        input.loaded.map(Optional.init).bindTo(viewModel.frienji).addDisposableTo(disposeBag)
        
        keepButton.rx_tap.asObservable().bindNext { [unowned self] _ in
            self.animateProfileCardDown()
        }.addDisposableTo(self.disposeBag)
        
        discardButton.rx_tap.asObservable().bindNext { [unowned self] _ in
            self.animateProfileCardUp()
        }.addDisposableTo(self.disposeBag)
    }
    
    // MARK: Pan gesture
    
    private func addPanGestureToFrienjiCard() {
        self.profile.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(PreviewViewController.onProfileCardPan(_:))))
    }
    
    func onProfileCardPan(gesture:UIPanGestureRecognizer) {
        let translation = gesture.translationInView(gesture.view)
        
        switch (gesture.state) {
            
        case .Began, .Changed:
            self.profile.transform = CGAffineTransformMakeTranslation(0, translation.y)
            
            self.keepLabel.hidden = translation.y < 0
            self.discardLabel.hidden = translation.y > 0
            
            self.view.layoutIfNeeded()
            
        case .Ended:
            if abs(translation.y) < self.view.frame.size.height / 4 {
                self.animateProfileCardToStartingPosition()
                
            } else if translation.y < 0 {
                self.animateProfileCardUp()
                
            } else {
                self.animateProfileCardDown()
            }
            
        default:
            break
        }
    }
    
    // MARK: Animation
    
    private func animateProfileCardDown() {
        UIView.animateWithDuration(kDefaultAnimationDuration, animations: {
            self.profile.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height)
        }) { (result:Bool) in
            self.viewModel.catched.onNext()
        }
    }
    
    private func animateProfileCardUp() {
        UIView.animateWithDuration(kDefaultAnimationDuration, animations: {
            self.profile.transform = CGAffineTransformMakeTranslation(0, -self.view.frame.size.height)
        }) { (result:Bool) in
            self.viewModel.rejected.onNext()
        }
    }
    
    private func animateProfileCardToStartingPosition() {
        UIView.animateWithDuration(kDefaultAnimationDuration, animations: {
            self.profile.transform = CGAffineTransformIdentity
        })
    }

}
