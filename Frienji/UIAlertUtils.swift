//
//  UIAlertUtils.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class UIAlertUtils {
    
    class func showAlertWithTitle(title:String, fromController:UIViewController, showCompletion:(() -> Void)?) {
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: Localizations.alert.ok_button, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            alert.dismissViewControllerAnimated(true, completion: nil)
        }
        
        alert.addAction(okAction)
        
        fromController.presentViewController(alert, animated: true, completion: showCompletion)
    }
    
    class func showAlertWithTitle(title:String, message:String, fromController:UIViewController, showCompletion:(() -> Void)?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: Localizations.alert.ok_button, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            alert.dismissViewControllerAnimated(true, completion: nil)
        }
        
        alert.addAction(okAction)
        
        fromController.presentViewController(alert, animated: true, completion: showCompletion)
    }
    
    class func showAlertWithTitle(title:String, message:String?, positiveButton:String, negativeButton:String, fromController:UIViewController, showCompletion:(() -> Void)?, actionHandler:((possitiveButtonTouched:Bool) -> Void)?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let positiveAction = UIAlertAction(title: positiveButton, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            actionHandler?(possitiveButtonTouched: true)
        }
        
        let negativeAction = UIAlertAction(title: negativeButton, style: UIAlertActionStyle.Cancel) { (action:UIAlertAction) -> Void in
            
            actionHandler?(possitiveButtonTouched: false)
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        
        fromController.presentViewController(alert, animated: true, completion: showCompletion)
    }
    
    class func showAlertWithTitle(title:String, message:String?, positiveButton:String, fromController:UIViewController, showCompletion:(() -> Void)?, actionHandler:(() -> Void)?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let positiveAction = UIAlertAction(title: positiveButton, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            actionHandler?()
        }
        
        alert.addAction(positiveAction)
        
        fromController.presentViewController(alert, animated: true, completion: nil)
    }

    class func showApiError(error: ErrorType, fromController controller: UIViewController) {
        #if DEBUG
            print("API error: \(error)")
        #endif
        if let statusCode = FrienjiApiError.fromError(error).statusCode where FrienjiApiError.HttpStatusCode(rawValue: statusCode) == .Unauthorized {
            UIAlertUtils.showAlertOnAuthanticationFailed(error, fromController: controller)
        }
        UIAlertUtils.showAlertWithTitle(Localizations.alert.errorTitle, message: FrienjiApiError.fromError(error).message, fromController: controller, showCompletion: nil)
    }
    
    class func showAlertOnAuthanticationFailed(error: ErrorType, fromController controller: UIViewController) {
        UIAlertUtils.showAlertWithTitle(Localizations.alert.errorTitle, message: FrienjiApiError.fromError(error).message, positiveButton: Localizations.signUp.login_on_frenji, fromController: controller, showCompletion: nil, actionHandler: {
            let settings = Settings.sharedInstance
            settings.sessionToken = nil
            settings.sessionClient = nil
            settings.sessionUID = nil
            settings.userVerified = false
            let navigationController = FrienjiNavigationController(rootViewController: IntroductionViewController.loadFromStoryboard())
            navigationController.navigationBarHidden = true
            if let appDelegate = UIApplication.sharedApplication().delegate, let window = appDelegate.window {
                window?.rootViewController = navigationController
            }
        })
    }

}

// MARK: - Rx helpers

extension ObservableConvertibleType {

    func showAlertOnApiError(fromController: UIViewController) -> Observable<E> {
        return self.asObservable()
            .doOnError { [unowned fromController] error in
                UIAlertUtils.showApiError(error, fromController: fromController)
        }
    }

}
