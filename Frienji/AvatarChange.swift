//
//  AvatarChange.swift
//  Frienji
//
//  Created by Adam Szeremeta on 01.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct AvatarChange {
    
    let id: Int
    let createdAt: NSDate
    let authorName: String
    let oldAvatar: Avatar
    let newAvatar: Avatar
}

// MARK: - Decode

extension AvatarChange: Decodable {
    
    static func decode(json: JSON) -> Decoded<AvatarChange> {
        return curry(AvatarChange.init)
            <^> json <| "id"
            <*> (json <| "created_at" >>- toNSDate)
            <*> json <| ["author", "username"]
            <*> json <| ["avatar_trail", "old_avatar"]
            <*> json <| ["avatar_trail", "new_avatar"]
    }
    
}
