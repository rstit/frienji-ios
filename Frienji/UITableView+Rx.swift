//
//  UITableView+Rx.swift
//  Frienji
//
//  Created by Piotr Łyczba on 31/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift

extension UITableView {

    func rx_cellSelected(cellIdentifier: String) -> Observable<UITableViewCell> {
        return rx_itemSelected
            .map(cellForRowAtIndexPath)
            .flatMap { cell -> Observable<UITableViewCell> in
                guard let cell = cell where cell.reuseIdentifier == cellIdentifier else {
                    return .empty()
                }

                return .just(cell)
            }
    }

}
