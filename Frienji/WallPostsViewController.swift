//
//  WallPostsViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 05/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

class WallPostsViewController: WallViewController, StoryboardLoad {

    // MARK: - Storyboard load
    
    static var storyboardId: String = "Main"
    static var storyboardControllerId = "WallPostsViewController"

    // MARK: - Outlets

    @IBOutlet weak var profile: ProfileView!
    @IBOutlet var profileAvatarTapGesture: UITapGestureRecognizer!

    private var shouldReloadPosts = false
    
    override var viewModel: Observable<WallViewModel> {
        return WallViewModel.create(
            withProfileLoaded: input.loaded,
            textSent: textSent,
            imageAttached: attachmentBehavior,
            locationAttached: locationBehavior,
            loadMoreTriggered: tableView.loadMoreTrigger,
            liked: postLiked,
            disliked: postDisliked,
            reported: postReported,
            viewAppeared: viewAppeared
        )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //remove settings button if we are not in our wall
        self.input.loaded.asObservable().subscribeNext({ [unowned self] frienji in
            if frienji.dbID == Settings.sharedInstance.userFrienji?.dbID {
                self.profile.avatarImage.addGestureRecognizer(self.profileAvatarTapGesture)
            }
        }).addDisposableTo(self.disposeBag)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if shouldReloadPosts {
            self.viewAppeared.on(.Next())
            self.shouldReloadPosts = false
        }
    }

    override func createBindings() {
        super.createBindings()

        input.loaded.bindTo(profile.profileLoaded).addDisposableTo(disposeBag)

        postCommented
            .subscribeNext { [unowned self] post in
                self.performSegueWithIdentifier(R.segue.wallPostsViewController.showComments.identifier, sender: Observable.just(post))
            }
            .addDisposableTo(disposeBag)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if let wallCommentsViewController = segue.destinationViewController as? WallCommentsViewController, post = sender as? Observable<Post> {
            post.bindTo(wallCommentsViewController.postLoaded).addDisposableTo(disposeBag)
            input.loaded.bindTo(wallCommentsViewController.input.loaded).addDisposableTo(disposeBag)
            wallCommentsViewController.view.backgroundColor = view.backgroundColor
            
            self.shouldReloadPosts = true
        }
    }
    
    //MARK: - Actions
    
    @IBAction func blockFrienjiButtonPressed(sender: AnyObject) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let blockFrienjiAction = UIAlertAction(title: Localizations.wall.blockFrienji.destructiveButtonTitle, style: UIAlertActionStyle.Destructive) { [weak self] (action:UIAlertAction) -> Void in
            self?.activityIndicator.startAnimating()
            self?.blockFrienji()
        }
        
        let negativeAction = UIAlertAction(title: Localizations.wall.blockFrienji.cancelButtonTitle, style: UIAlertActionStyle.Cancel) { (action:UIAlertAction) -> Void in
        }
        
        alert.addAction(blockFrienjiAction)
        alert.addAction(negativeAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    private func blockFrienji() {
        input.loaded.asObservable().subscribeNext({ [unowned self] frienji in
            FrienjiApi.sharedInstance.blockFrienji(frienji).showAlertOnApiError(self).catchError { [weak self] _ in
                self?.activityIndicator.stopAnimating()
                return Observable.empty()
                }.asObservable().subscribeNext({ [weak self] in
                    self?.activityIndicator.stopAnimating()
                    self?.showUnblockHintView()
                }).addDisposableTo(self.disposeBag)
            }).addDisposableTo(disposeBag)
    }
    
    private func showUnblockHintView() {
        self.performSegue(with: R.segue.wallPostsViewController.showUnblockFrienjiHint.identifier) { (segue) in
            if let unblockHintViewController = segue.destinationViewController as? UnblockHintViewController {
                unblockHintViewController.delegate = self
            }
        }
    }
}

extension WallPostsViewController: UnblockHintViewControllerDelegate {
    func unblockHintViewController(unblockHintViewController: UnblockHintViewController, tappedWithSender sender: AnyObject) {
        unblockHintViewController.dismissViewControllerAnimated(true) { [unowned self] in
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
