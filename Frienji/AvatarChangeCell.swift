//
//  AvatarChangeCell.swift
//  Frienji
//
//  Created by Adam Szeremeta on 01.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class AvatarChangeCell: UITableViewCell, NibLoad, CellReuseIdentifier {
    
    static let kEstimatedViewHeight: CGFloat = 44
    
    //outlets
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var messageAvatar: UIImageView!
    
    // MARK: Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clearColor()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.messageAvatar.image = nil
    }

    // MARK: Data
    
    func configureWithAvatarChange(change:AvatarChange) {
        //avatar
        setAvatarContentMode(change.newAvatar)
        self.messageAvatar.image = change.newAvatar.avatarImage

        //description
        setDescriptionForUsername(change.authorName)
    }
    
    func configureWithPost(post:Post) {
        //avatar
        if let avatarChange = post.avatarTrail {
            setAvatarContentMode(avatarChange.newAvatar)
        }
        self.messageAvatar.image = post.avatarTrail?.newAvatar.avatarImage
        
        //description
        setDescriptionForUsername(post.author?.username ?? "-")
    }
    
    // MARK: Helpers
    
    private func setDescriptionForUsername(username:String) {
        self.messageLabel.attributedText = Localizations.chat.is_now_a(username)
            .attributedString()
            .textColor(UIColor.blackColor())
            .font(UIFont.latoRegulatWithSize(13.0))
            .font(UIFont.latoBoldWithSize(13.0), forText: username)
    }
    
    private func setAvatarContentMode(newAvatar:Avatar) {
        self.messageAvatar.contentMode = newAvatar.type == AvatarType.AnimatedImage ? .ScaleToFill : .ScaleAspectFit
    }
    
}
