//
//  BaseMessageViewController.swift
//  Frienji
//
//  Created by adam kolodziej on 08.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import RxSwift
import MapKit

class BaseMessageViewController: BaseViewController, HasDisposeBag {
    
    @IBOutlet internal weak var messageToolbar:MessageToolbarView!
    @IBOutlet weak var toolbarContainerBottomConstraint: NSLayoutConstraint!
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageToolbar.delegate = self
    }
    
    // MARK: Keyboard
    
    override func keyboardWillChangeSize(newSize size: CGSize) {
        self.toolbarContainerBottomConstraint.constant = size.height
        
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func keyboardWillHide() {
        self.toolbarContainerBottomConstraint.constant = 0
        
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }
}

extension BaseMessageViewController: MessageToolbarProtocol {
    
    func didTouchSendWithMesssage(message: String, forMessageToolBarView messageToolBarView: MessageToolbarView) {
        
    }
    
    func didTouchCamera(forMessageToolBarView messageToolBarView:MessageToolbarView) {
        UIActionSheetUtils.showMediaPickingActionSheet(fromController: self, actionCallback: { [unowned self] (selectedAction) in
            switch (selectedAction) {
                
            case .Photos:
                self.presentImagePickerForType(UIImagePickerControllerSourceType.PhotoLibrary)
                
            case .Camera:
                self.presentImagePickerForType(UIImagePickerControllerSourceType.Camera)
            }
        }, showCompletion: nil)
    }
    
    func didTouchLocation(forMessageToolBarView messageToolBarView:MessageToolbarView) {
        let locationPickerViewController = LocationPickerViewController.loadFromStoryboard()
        locationPickerViewController.delegate = self
        showPopover(withViewController:locationPickerViewController)
    }
    
    private func presentImagePickerForType(type: UIImagePickerControllerSourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.mediaTypes = [String(kUTTypeImage)]
        picker.sourceType = type
        
        presentViewController(picker, animated: true, completion: nil)
    }
    
    private func showPopover(withViewController controller: UIViewController) {
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.titleTextAttributes = self.navigationController?.navigationBar.titleTextAttributes
        nav.modalPresentationStyle = UIModalPresentationStyle.Popover
        nav.popoverPresentationController?.delegate = self
        if let popover = controller.popoverPresentationController {
            popover.delegate = self
            popover.sourceView = self.messageToolbar
        }
        nav.popoverPresentationController?.delegate = self
        presentViewController(nav, animated: true, completion: nil)
    }
}

extension BaseMessageViewController: LocationPickerDelegate {
    func locationPicker(picker: LocationPickerViewController, didFinishPickingLocation location: CLLocationCoordinate2D, withMessage message:String) {
        
    }
}

extension BaseMessageViewController: SendPhotoViewControllerDelegate {
    func sendPhotoController(controller: SendPhotoViewController, didFinishPreparingPhoto photo: UIImage, withMessage message: String) {
    }
}
    
extension BaseMessageViewController: UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.dismissViewControllerAnimated(true) {
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                let sendPhotoViewController = SendPhotoViewController.loadFromStoryboard()
                sendPhotoViewController.delegate = self
                sendPhotoViewController.photo = pickedImage
                self.showPopover(withViewController:sendPhotoViewController)
            }
        }
    }
}

extension BaseMessageViewController: UIPopoverPresentationControllerDelegate, UINavigationControllerDelegate {
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.FullScreen
    }
    
    func presentationController(controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        if let navigationController = controller.presentedViewController as? UINavigationController, let topViewController = navigationController.topViewController {
            let btnDone = UIBarButtonItem(title: Localizations.modalView.dismissButton, style: .Done, target: nil, action: nil)
            btnDone.rx_tap.asObservable().bindNext({ [weak self] _ in
                self?.dismissModalView()
            }).addDisposableTo(disposeBag)
            topViewController.navigationItem.leftBarButtonItem = btnDone
            topViewController.navigationItem.title = controller.presentedViewController.title
            return navigationController
        }
        
        return controller.presentedViewController
    }
    
    private func dismissModalView() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
