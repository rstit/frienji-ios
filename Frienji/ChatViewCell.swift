//
//  ChatViewCell.swift
//  Frienji
//
//  Created by Adam Szeremeta on 21.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import RxSwift
import CoreLocation

protocol ChatViewCellProtocol: class {
    
    func chatViewCellDidLongPress(cell:ChatViewCell) -> Void
    func chatViewCellDidTouchImageAttachment(cell:ChatViewCell, imageView:UIImageView) -> Void
    func chatViewCellDidTouchLocationAttachment(cell:ChatViewCell, imageView:UIImageView) -> Void
}

class ChatViewCell: UITableViewCell, NibLoad, CellReuseIdentifier {
    
    static let kEstimatedViewHeight: CGFloat = 54
    static let kContentImageHeight: CGFloat = 160
    static let kContentTextPadding: CGFloat = 4
    
    static let kSelectionViewWidth: CGFloat = 50
    
    //outlets
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var leftAvatarContainer: UIView!
    @IBOutlet weak var leftAvatar: UIImageView!
    @IBOutlet weak var leftMessage: UILabel!
    @IBOutlet weak var leftViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftMessageBackground: UIImageView!
    @IBOutlet weak var leftContentImageView: UIImageView!
    @IBOutlet weak var leftContentImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftContentSpaceToTextConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var rightAvatarContainer: UIView!
    @IBOutlet weak var rightAvatar: UIImageView!
    @IBOutlet weak var rightMessage: UILabel!
    @IBOutlet weak var rightViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightMessageBackground: UIImageView!
    @IBOutlet weak var rightContentImageView: UIImageView!
    @IBOutlet weak var rightContentImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightContentSpaceToTextConstraint: NSLayoutConstraint!
 
    @IBOutlet weak var selectionContainerView: UIView!
    @IBOutlet weak var selectionButton: UIButton!
    
    //properties
    weak var delegate: ChatViewCellProtocol?
    
    private var leftContentImageTapGesture: UITapGestureRecognizer?
    private var rightContentImageTapGesture: UITapGestureRecognizer?
    private var isDisplayingLocation: Bool = false
    
    private var disposeBag = DisposeBag()
    
    // MARK: Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clearColor()
        
        addLongPressGestureToCell()
        addTapGestureToAttachments()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.contentView.layoutIfNeeded()
        self.leftAvatarContainer.layer.cornerRadius = self.leftAvatarContainer.frame.size.height / 2
        self.rightAvatarContainer.layer.cornerRadius = self.rightAvatarContainer.frame.size.height / 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.leftAvatar.image = nil
        self.rightAvatar.image = nil
        
        self.leftContentImageView.sd_cancelCurrentImageLoad()
        self.rightContentImageView.sd_cancelCurrentImageLoad()
        
        self.leftContentImageView.image = nil
        self.rightContentImageView.image = nil
        
        self.disposeBag = DisposeBag()
        
        self.setSelectionModeEnabled(false)
    }
    
    // MARK: Selection
    
    func setSelectionModeEnabled(enabled:Bool) {
        let transform = CGAffineTransformMakeTranslation(enabled ? -ChatViewCell.kSelectionViewWidth : 0, 0)
        
        self.rightView.transform = transform
        self.selectionContainerView.transform = transform
        self.layoutIfNeeded()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.selectionButton.selected = selected
    }
    
    // MARK: Long press
    
    private func addLongPressGestureToCell() {
        self.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(ChatViewCell.onCellLongPress)))
    }
    
    func onCellLongPress() {
        self.delegate?.chatViewCellDidLongPress(self)
    }
    
    // MARK: Attachment touch
    
    private func addTapGestureToAttachments() {
        let leftGesture = UITapGestureRecognizer(target: self, action: #selector(ChatViewCell.onAttachmentTouch(_:)))
        self.leftContentImageView.addGestureRecognizer(leftGesture)
        self.leftContentImageTapGesture = leftGesture

        let rightGesture = UITapGestureRecognizer(target: self, action: #selector(ChatViewCell.onAttachmentTouch(_:)))
        self.rightContentImageView.addGestureRecognizer(rightGesture)
        self.rightContentImageTapGesture = rightGesture
    }
    
    func onAttachmentTouch(gesture:UITapGestureRecognizer) {
        
        guard let view = gesture.view as? UIImageView else {
            return
        }
        
        self.isDisplayingLocation ?
            self.delegate?.chatViewCellDidTouchLocationAttachment(self, imageView: view) :
            self.delegate?.chatViewCellDidTouchImageAttachment(self, imageView: view)
    }
    
    // MARK: Data
    
    func configureWithMessage(message:Message?, andUser user:Frienji?) {
        
        guard let message = message, let frenji = user else {
            return
        }
        
        message.isAuthor ? self.configureForAuthorWithMessage(message, andUser: frenji) : self.configureForSomeoneElseWithMessage(message, andUser: frenji)
        
        self.leftView.hidden = message.isAuthor
        self.rightView.hidden = !message.isAuthor
        self.leftViewBottomConstraint.priority = !message.isAuthor ? 750 : 250
        self.rightViewBottomConstraint.priority = message.isAuthor ? 750 : 250
        
        message.hasLocation ? self.setImageForMessageLocation(message) : self.setImageForMessageContent(message)
        
        self.updateConstraints()
        self.layoutIfNeeded()
    }
    
    private func configureForAuthorWithMessage(message:Message, andUser user:Frienji) {
        let messageContent = message.content ?? ""
        
        self.rightMessage.text = messageContent
        self.rightAvatar.image = user.avatar.avatarImage
        self.rightAvatar.contentMode = user.avatar.type == AvatarType.AnimatedImage ? .ScaleToFill : .ScaleAspectFit
        self.rightContentSpaceToTextConstraint.constant = messageContent.isEmpty ? 0 : ChatViewCell.kContentTextPadding
        
        //content
        self.setAuthorContentImageHidden(false)
        self.setSomeoneElseContentImageHidden(true)
    }
    
    private func configureForSomeoneElseWithMessage(message:Message, andUser user:Frienji) {
        let messageContent = message.content ?? ""

        self.leftMessage.text = messageContent
        self.leftAvatar.image = user.avatar.avatarImage
        self.leftAvatar.contentMode = user.avatar.type == AvatarType.AnimatedImage ? .ScaleToFill : .ScaleAspectFit
        self.leftContentSpaceToTextConstraint.constant = messageContent.isEmpty ? 0 : ChatViewCell.kContentTextPadding

        //content
        self.setAuthorContentImageHidden(true)
        self.setSomeoneElseContentImageHidden(false)
    }
    
    // MARK: Helpers
    
    private func setImageForMessageLocation(message:Message?) {
        let isForAuthor = (message?.isAuthor ?? false)
        
        guard let latitude = message?.latitude, let longitude = message?.longitude else {
            self.setAuthorContentImageHidden(true)
            self.setSomeoneElseContentImageHidden(true)
            return
        }
        
        let view = isForAuthor ? self.rightContentImageView : self.leftContentImageView
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        location.mapSnapshot.map(Optional.init).cachedImageForLocation(location).bindTo(view.rx_image).addDisposableTo(self.disposeBag)
    }
    
    private func setImageForMessageContent(message:Message?) {
        let isForAuthor = (message?.isAuthor ?? false)

        guard let image = message?.attachmentImageUrl, let imageURL = NSURL(string: image) where !image.isEmpty else {
            self.setAuthorContentImageHidden(true)
            self.setSomeoneElseContentImageHidden(true)
            return
        }
        
        let view = isForAuthor ? self.rightContentImageView : self.leftContentImageView

        //load image
        view.sd_setImageWithURL(imageURL)
    }
    
    private func setSomeoneElseContentImageHidden(hidden:Bool) {
        self.leftContentImageView.hidden = hidden
        self.leftContentImageViewHeightConstraint.constant = hidden ? 0 : ChatViewCell.kContentImageHeight
    }
    
    private func setAuthorContentImageHidden(hidden:Bool) {
        self.rightContentImageView.hidden = hidden
        self.rightContentImageViewHeightConstraint.constant = hidden ? 0 : ChatViewCell.kContentImageHeight
    }
    
}
