//
//  ArSliderView.swift
//  Frienji
//
//  Created by Adam Szeremeta on 06.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ArSliderView: UIView, NibLoad {
    
    static let kMinSliderDistanceValue: CGFloat = 500 // 500 m
    static let kMiddleSliderDistanceValue: CGFloat = 1000 * 1000 // 1000 km
    static let kMaxSliderDistanceValue: CGFloat = 1000 * 25000 // 25K km
    
    private let kSliderBarRightOffset: CGFloat = 22
    private let kSliderDotAreaSize: CGFloat = 44
    private let kSliderBarWidth: CGFloat = 4
    private let kSliderDotTopBottomPadding: CGFloat = 6
    private let kSliderDotRadius: CGFloat = 8
    
    //outlets
    @IBOutlet private weak var hereButton: UIButton!
    @IBOutlet private weak var farButton: UIButton!
    @IBOutlet private weak var infinityButton: UIButton!
    
    @IBOutlet private weak var sliderDotImageView: UIImageView!
    @IBOutlet private weak var sliderDotBottomConstraint: NSLayoutConstraint!
    
    //properties
    let sliderDistance = Variable<CGFloat>(ArSliderView.kMinSliderDistanceValue)
    
    private var sliderDotPanGesture: UIPanGestureRecognizer!
    
    private var currentAnimationDelta: CGFloat = 0
    private var animationDisposeBag = DisposeBag()
    
    private var maxSliderOffset: CGFloat {
        return self.frame.size.height - kSliderDotAreaSize
    }
    
    private var middleSliderOffset: CGFloat {
        return self.maxSliderOffset / 2
    }
    
    private var minSliderOffset: CGFloat {
        return 0
    }
    
    // MARK: Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        setUpButtonsAppearance()
        addPanGestureToSliderDot()
        addTapGestureToView()        
    }
    
    // MARK: Appearance
    
    private func setUpButtonsAppearance() {
        let strokeTextAttributes = [
            NSStrokeColorAttributeName: UIColor.appPurpleColor(),
            NSStrokeWidthAttributeName: -1,
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont.latoBoldWithSize(15.0)
        ]

        let infinityString = Localizations.ar.slider.infinity
        let infinityAttributedString = NSMutableAttributedString(string: infinityString, attributes: strokeTextAttributes)
        infinityAttributedString.addAttributes([NSFontAttributeName: UIFont.latoBoldWithSize(22.0)], range: (infinityString as NSString).rangeOfString(infinityString))
        
        self.infinityButton.setAttributedTitle(infinityAttributedString, forState: UIControlState.Normal)
        self.farButton.setAttributedTitle(NSAttributedString(string: Localizations.ar.slider.far, attributes: strokeTextAttributes), forState: UIControlState.Normal)
        self.hereButton.setAttributedTitle(NSAttributedString(string: Localizations.ar.slider.here, attributes: strokeTextAttributes), forState: UIControlState.Normal)
    }
    
    // MARK: Actions
    
    @IBAction func onInfinityButtonTouch(sender: AnyObject) {
        self.animateDistanceToNewValueForNewOffset(self.maxSliderOffset)

        self.sliderDotBottomConstraint.constant = self.maxSliderOffset
        
        UIView.animateWithDuration(kDefaultAnimationDuration) { 
            self.layoutIfNeeded()
        }
    }
    
    @IBAction func onFarButtonTouch(sender: AnyObject) {
        self.animateDistanceToNewValueForNewOffset(self.middleSliderOffset)

        self.sliderDotBottomConstraint.constant = self.middleSliderOffset
        
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.layoutIfNeeded()
        }
    }
    
    @IBAction func onHereButtonTouch(sender: AnyObject) {
        self.animateDistanceToNewValueForNewOffset(self.minSliderOffset)

        self.sliderDotBottomConstraint.constant = self.minSliderOffset
        
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.layoutIfNeeded()
        }
    }
    
    // MARK: Gestures (UITapGesture)
    
    private func addTapGestureToView() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(ArSliderView.onViewTap(_:)))
        gesture.requireGestureRecognizerToFail(self.sliderDotPanGesture)
        
        self.addGestureRecognizer(gesture)
    }
    
    func onViewTap(gesture:UITapGestureRecognizer) {
        let location = self.frame.size.height - kSliderDotAreaSize/2 - gesture.locationInView(self).y
        let offset = max(self.minSliderOffset, min(location, self.maxSliderOffset))
        
        //animate
        self.animateDistanceToNewValueForNewOffset(offset)
        self.sliderDotBottomConstraint.constant = offset
        
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.layoutIfNeeded()
        }
    }
    
    // MARK: Gestures (UIPanGesture)
    
    private func addPanGestureToSliderDot() {
        self.sliderDotPanGesture = UIPanGestureRecognizer(target: self, action: #selector(ArSliderView.onDotSlide(_:)))
        self.sliderDotImageView.addGestureRecognizer(self.sliderDotPanGesture)
    }
    
    func onDotSlide(gesture:UIPanGestureRecognizer) {
        switch (gesture.state) {
            
        case .Began, .Changed:
            let translation = -gesture.translationInView(self).y

            let sliderOffset = min(self.maxSliderOffset, max(self.minSliderOffset, self.sliderDotBottomConstraint.constant + translation))
            self.sliderDotBottomConstraint.constant = sliderOffset
            
            self.calculateSliderDistanceForOffset(sliderOffset)
            
            gesture.setTranslation(CGPointZero, inView: self)
            
        default:
            break
        }
    }
    
    // MARK: Slider distance
    
    private func calculateSliderDistanceForOffset(offset:CGFloat) {
        switch (offset) {
            
        case self.minSliderOffset...self.middleSliderOffset:
            let distance = ArSliderView.kMinSliderDistanceValue + ArSliderView.kMiddleSliderDistanceValue * offset / self.middleSliderOffset

            self.sliderDistance.value = min(ArSliderView.kMiddleSliderDistanceValue, distance)
            
        case self.middleSliderOffset...self.maxSliderOffset:
            let reducedOffset = max(0, offset - self.middleSliderOffset)
            let reducedMaxOffset = self.maxSliderOffset - self.middleSliderOffset
            let distance = ArSliderView.kMiddleSliderDistanceValue + ArSliderView.kMaxSliderDistanceValue * reducedOffset / reducedMaxOffset
            
            self.sliderDistance.value = min(ArSliderView.kMaxSliderDistanceValue, distance)
            
        default:
            break
        }
    }
    
    // MARK: Value "animation"
    
    private func animateDistanceToNewValueForNewOffset(newOffset:CGFloat) {
        let currentOffset = self.sliderDotBottomConstraint.constant
        
        //do smooth transition between old and new value
        self.animationDisposeBag = DisposeBag()
        let animationDelta: CGFloat = 0.05
        self.currentAnimationDelta = 0
        
        Observable<Int>.interval(0.03, scheduler: MainScheduler.instance).bindNext({ [weak self] _ in
            self?.currentAnimationDelta += animationDelta
            
            if let strongSelf = self where strongSelf.currentAnimationDelta < 1 {
                let offset = currentOffset + strongSelf.currentAnimationDelta * (newOffset - currentOffset)
                strongSelf.calculateSliderDistanceForOffset(offset)
            } else {
                self?.calculateSliderDistanceForOffset(self?.sliderDotBottomConstraint.constant ?? 0)
                self?.animationDisposeBag = DisposeBag()
            }
            
        }).addDisposableTo(self.animationDisposeBag)
    }
    
    // MARK: Draw
    
    override func drawRect(rect: CGRect) {

        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        CGContextSetFillColorWithColor(context, UIColor.whiteColor().CGColor)
        
        //draw bar for slider and 3 dots, top, bottom and middle
        let sliderRect = CGRectMake(rect.width - kSliderBarRightOffset, kSliderDotAreaSize / 2, kSliderBarWidth, rect.height - kSliderDotAreaSize)
        let sliderPath = UIBezierPath(roundedRect: sliderRect, cornerRadius: kSliderBarWidth / 2)
        sliderPath.fill()
        
        //dots
        let dotXPosition = rect.width - kSliderBarRightOffset - kSliderBarWidth / 2
        
        let topDot = UIBezierPath(ovalInRect: CGRectMake(dotXPosition, kSliderDotAreaSize / 2, kSliderDotRadius, kSliderDotRadius))
        let middleDot = UIBezierPath(ovalInRect: CGRectMake(dotXPosition, rect.height / 2 - kSliderDotRadius / 2, kSliderDotRadius, kSliderDotRadius))
        let bottomDot = UIBezierPath(ovalInRect: CGRectMake(dotXPosition, rect.height - kSliderDotAreaSize / 2 + kSliderDotTopBottomPadding - kSliderDotRadius, kSliderDotRadius, kSliderDotRadius))

        topDot.fill()
        middleDot.fill()
        bottomDot.fill()
    }
    
}
