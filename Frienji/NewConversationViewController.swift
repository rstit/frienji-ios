//
//  NewConversationViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 19/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import SegueKit
import CenterAlignedCollectionViewFlowLayout

class NewConversationViewController: UIViewController {

    //outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var dismiss: UIBarButtonItem!

    //properties
    let disposeBag = DisposeBag()

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        if let layout = self.getCenterAlignedLayout() {
            self.collectionView.collectionViewLayout = layout
        }
        
        // Initial load
        let loadCommand = FrienjiApi.sharedInstance.getFrienjis(collectionView.loadMoreTrigger)
            .showAlertOnApiError(self)
            .asDriver(onErrorJustReturn: [])
            .map(ZooCommand.LoadFrienjis)

        // View model
        let viewModel = loadCommand
            .scan(ZooViewModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }

        // Bind view model
        viewModel.map { viewModel in
            viewModel.frienjis
        }
        .drive(collectionView.rx_itemsWithCellIdentifier(R.reuseIdentifier.zooCollectionViewCellIdentifier.identifier)) { (row, frienji, cell: ZooCollectionViewCell) in
            cell.confireCellForFrienji(frienji, isViewInEditionMode: false)
        }
        .addDisposableTo(disposeBag)

        viewModel.driveNext { [weak self] _ in
            self?.loadingIndicator.hidden = true
        }.addDisposableTo(self.disposeBag)
        
        viewModel.asObservable().map { $0.frienjis.count != 0 }.bindTo(self.emptyLabel.rx_hidden).addDisposableTo(self.disposeBag)
        viewModel.asObservable().map { $0.frienjis.count == 0 }.bindTo(self.collectionView.rx_hidden).addDisposableTo(self.disposeBag)
        
        // Navigation
        let navigationHandler = NavigationHandler(sourceViewController: self)
        collectionView.rx_modelSelected(Frienji.self)
            .flatMap { [unowned self] frienji in
                FrienjiApi.sharedInstance.createConversation(frienji).showAlertOnApiError(self).catchError { _ in Observable.empty() }
            }
            .bindTo(navigationHandler.modalDismissed(InboxViewController.self)) { input, conversation in
                input.newConversationStarted.onNext(conversation)
            }
            .addDisposableTo(disposeBag)
        dismiss.rx_tap
            .bindTo(navigationHandler.modalDismissed(InboxViewController.self)) { _ in }
            .addDisposableTo(disposeBag)
    }
    
    // MARK: Layout
    
    private func getCenterAlignedLayout() -> UICollectionViewLayout? {
        
        guard let oldLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return nil
        }
        
        let layout = CenterAlignedCollectionViewFlowLayout()
        layout.minimumInteritemSpacing = oldLayout.minimumInteritemSpacing
        layout.minimumLineSpacing = oldLayout.minimumLineSpacing
        layout.headerReferenceSize = oldLayout.headerReferenceSize
        layout.itemSize = oldLayout.itemSize
        layout.sectionInset = oldLayout.sectionInset
        
        return layout
    }

}
