//
//  ActivityCell.swift
//  Frienji
//
//  Created by Piotr Łyczba on 26/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {

    static let kEstimatedViewHeight: CGFloat = 90
    
    //outlets
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var createdAt: UILabel!

    // MARK: Life cycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.avatar.image = nil
        self.details.text = nil
        self.details.attributedText = nil
    }
    
    // MARK: Data
    
    func configure(activity: Activity<Any>) {
        self.avatar.image = activity.owner.avatar.avatarImage
        self.createdAt.text = activity.createdAt.timeAgo
        
        setDetailsTextFor(activity)
        setSelectionStyleFor(activity)
    }
    
    private func setDetailsTextFor(activity:Activity<Any>) {
        
        guard let message = (activity.entity as? Like)?.postMessage else {
            self.details.text = String(format: NSLocalizedString(activity.key.rawValue, comment: ""), activity.owner.username)
            return
        }
        
        let ownerText = String(format: NSLocalizedString(activity.key.rawValue, comment: ""), activity.owner.username)
        let attributedText = "\(ownerText)\n\n\"\(message)\"".attributedString().textColor(UIColor.blackColor()).font(UIFont.latoRegulatWithSize(13.0), forText: ownerText).font(UIFont.latoItalicWithSize(13.0), forText: message)
        
        self.details.attributedText = attributedText
    }
    
    private func setSelectionStyleFor(activity:Activity<Any>) {
        switch activity.key {
            
        case .IncomingMessage, .Comment, .CommentOnMyWall, .LikeComment, .LikePost, .Post:
            self.selectionStyle = .Default
            
        default:
            self.selectionStyle = .None
        }
    }

}
