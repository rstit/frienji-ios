//
//  SettingsViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 27/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

// MARK: - Input

struct SettingsInput: Input {

    private var viewModel: SettingsViewModel? = nil

    var avatarSelected: AnyObserver<Avatar?>? {
        return viewModel?.avatarSelected.asObserver()
    }

}

class SettingsViewController: BaseViewController, HasInput, HasDisposeBag, StoryboardLoad {

    static var storyboardId: String = "Main"
    
    var input = SettingsInput()

    // MARK: - Outlets
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var bio: UITextView!
    @IBOutlet weak var cover: UIButton!
    @IBOutlet weak var avatar: UIButton!
    @IBOutlet weak var nameCharactersLeft: UILabel!
    @IBOutlet weak var bioCharactersLeft: UILabel!
    
    @IBOutlet weak var distanceUnit: UITextField!
    private var distancePicker: UIPickerView!
    
    @IBOutlet weak var settingsView: UIView!
    
    @IBOutlet weak var validationErrorView: UIView!
    @IBOutlet weak var validationErrorLabel: UILabel!
    @IBOutlet weak var validationErrorBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    private var saveButton: UIButton!
    
    // MARK: - View model

    var viewModel: SettingsViewModel! {
        didSet {
            input.viewModel = viewModel
            createBindings().forEach(disposeBag.addDisposable)

            viewModel.error
                .showAlertOnApiError(navigationController ?? self)
                .subscribe()
                .addDisposableTo(disposeBag)
        }
    }
    
    private var activityController:ActivityViewController?
    
    private var isKeyboardShown = false
    private var keyboardHeight:CGFloat?

    var disposeBag = DisposeBag()

    // MARK: - View's lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        createPickerViewForDistance()
        createBackButton()
        createSaveBarButtonItem()
        
        let navigationHandler = NavigationHandler(sourceViewController: self)
        let attachmentHandler = AttachmentHandler(targetViewController: self, cameraEnabled: true, galleryEnabled: true, locationEnabled: false)
        viewModel = SettingsViewModel(navigationHandler: navigationHandler, attachmentHandler: attachmentHandler)
        viewModel.initialized.onNext()
        
        if let imageView = cover.imageView {
            imageView.contentMode = UIViewContentMode.ScaleAspectFill
        }
        
        self.name.delegate = self
        self.bio.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.Settings) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.Settings) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.Settings)
            }
            coachmark.showWithAnimationInContainer()
        }
    }

    // MARK: - Bindings

    func createBindings() -> [Disposable] {
        return [
            name <-> viewModel.name,
            bio <-> viewModel.bio,
            viewModel.avatar --> avatar.rx_image,
            viewModel.cover --> cover.rx_image,
            avatar.rx_tap --> viewModel.avatarTapped,
            saveButton.rx_tap --> viewModel.saved,
            viewModel.isValid --> saveButton.rx_enabled,
            
            viewModel.name.asObservable().map { "\(SettingsViewModel.kNameValidationMaxCharactersCount - $0.characters.count)" }.bindTo(self.nameCharactersLeft.rx_text),
            viewModel.bio.asObservable().map { "\(SettingsViewModel.kBioValidationMaxCharactersCount - $0.characters.count)" }.bindTo(self.bioCharactersLeft.rx_text),
            
            viewModel.distanceUnit.asObservable().map { DistanceUnit.labels[$0.rawValue] }.bindTo(self.distanceUnit.rx_text),
            distancePicker.rx_itemSelected.map({ (row, component) -> DistanceUnit in
                return DistanceUnit(rawValue: row)!
            }).bindTo(viewModel.distanceUnit),
            
            viewModel.saveInProgress.asObservable().bindNext({ (inProgress:Bool) in
                if inProgress && self.activityController == nil {
                    self.activityController = ActivityViewController.getActivityInFullScreen("👾", inView: self.view)
                } else if !inProgress {
                    self.activityController?.hide()
                    self.activityController = nil
                }
            })
        ]
    }
    
    // MARK: Keyboard
    
    override func keyboardWillShowWithSize(keyboardSize: CGSize?, animationTime: Double) {
        if let height = keyboardSize?.height where !self.isKeyboardShown {
            self.scrollViewBottomConstraint.constant = height - self.settingsView.frame.size.height
            
            UIView.animateWithDuration(animationTime, animations: {
                self.view.layoutIfNeeded()
            })
            
            self.isKeyboardShown = true
            self.keyboardHeight = height
        }
    }
    
    override func keyboardWillChangeSize(newSize size: CGSize) {
        if self.isKeyboardShown {
            self.scrollViewBottomConstraint.constant = size.height - self.settingsView.frame.size.height
            
            UIView.animateWithDuration(kDefaultAnimationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        self.keyboardHeight = size.height
    }
    
    override func keyboardWillHide() {
        if self.isKeyboardShown {
            self.scrollViewBottomConstraint.constant = 0
            
            UIView.animateWithDuration(kDefaultAnimationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        self.isKeyboardShown = false
    }
    
    // MARK: Error view
    
    private func checkForErrorWithText(text:String, inFieldType fieldType:SettingsViewModel.FieldsValidation) -> Bool {
        
        guard let validationRules = SettingsViewModel.FieldsValidation.validationRules[fieldType] else {
            return true
        }
        
        let validationResult = text.characters.count <= validationRules.0

        if !validationResult && self.validationErrorBottomConstraint.constant <= 0 {
            self.showErrorView(withError: validationRules.1)
            
            //hide error after 3s
            performWithDelay(3, closure: { [weak self] in
                self?.hideErrorView()
            })
        }
        
        return validationResult
    }
    
    private func showErrorView(withError error:String) {
        self.validationErrorLabel.text = error
        self.view.layoutIfNeeded()
        self.validationErrorBottomConstraint.constant = -self.validationErrorView.frame.size.height
        self.view.layoutIfNeeded()

        self.validationErrorBottomConstraint.constant = self.keyboardHeight ?? 0
        
        UIView.animateWithDuration(kDefaultAnimationDuration) { 
            self.view.layoutIfNeeded()
        }
    }
    
    private func hideErrorView() {
        self.validationErrorBottomConstraint.constant = -self.validationErrorView.frame.size.height
        
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: Distance unit picker
    
    private func createPickerViewForDistance() {
        self.distancePicker = UIPickerView()
        self.distancePicker.backgroundColor = UIColor.whiteColor()
        self.distancePicker.dataSource = self
        self.distancePicker.delegate = self
        self.distancePicker.selectRow(Settings.sharedInstance.distanceUnit.rawValue, inComponent: 0, animated: false)
        
        self.distanceUnit.inputView = self.distancePicker
    }
    
    // MARK: Save button
    
    func createSaveBarButtonItem() {
        self.saveButton = UIButton(type: UIButtonType.Custom)
        self.saveButton.setTitle("👌", forState: UIControlState.Normal)
        self.saveButton.setTitle("👌🏽", forState: UIControlState.Highlighted)
        self.saveButton.sizeToFit()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.saveButton)
    }
    
    // MARK: Back button
    
    private func createBackButton() {
        let button = UIBarButtonItem(title: "🔙", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(SettingsViewController.onBackButtonTouch))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
        space.width = 4
        
        //left bar item has different padding then back button so we adding space
        self.navigationItem.leftBarButtonItems = [space, button]
    }
    
    func onBackButtonTouch() {
        self.view.endEditing(true)
        
        guard self.viewModel.changesPresent else {
            self.navigationController?.popViewControllerAnimated(true)
            return
        }
        
        UIAlertUtils.showAlertWithTitle(Localizations.settings.unsaved_changes, message: nil, positiveButton: Localizations.settings.unsaved_changes_save, negativeButton: Localizations.settings.unsaved_changes_discard, fromController: self, showCompletion: nil) { (possitiveButtonTouched) in
            if possitiveButtonTouched {
                self.viewModel.saved.onNext()
            } else {
                self.navigationController?.popViewControllerAnimated(true)
            }
        }
    }

}

// MARK: - Has attachment

extension SettingsViewController: HasAttachment {

    var attachmentRequested: Observable<Void> {
        return cover.rx_tap.asObservable()
    }

}

extension SettingsViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        switch textField {
            
        case self.name:
            self.nameCharactersLeft.hidden = false
            
        default:
            break
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let replacedText = NSString(string: textField.text ?? "").stringByReplacingCharactersInRange(range, withString: string)
        
        switch textField {
            
        case self.name:
            return self.checkForErrorWithText(replacedText, inFieldType: SettingsViewModel.FieldsValidation.Name)
            
        default:
            return true
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        switch textField {
            
        case self.name:
            self.nameCharactersLeft.hidden = true
            
        default:
            break
        }
        
        self.hideErrorView()
    }
}

extension SettingsViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        self.bioCharactersLeft.hidden = false
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        //check if "send" was hit
        guard text != "\n" else {
            self.view.endEditing(true)
            self.viewModel.saved.onNext()
            
            return false
        }
        
        
        let replacedText = NSString(string: textView.text ?? "").stringByReplacingCharactersInRange(range, withString: text)
        
        switch textView {
            
        case self.bio:
            return self.checkForErrorWithText(replacedText, inFieldType: SettingsViewModel.FieldsValidation.Bio)
            
        default:
            return true
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.bioCharactersLeft.hidden = true
        self.hideErrorView()
    }
    
}

extension SettingsViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return DistanceUnit.labels.count
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleAttributes = [
            NSFontAttributeName: UIFont.latoRegulatWithSize(14.0),
            NSForegroundColorAttributeName: UIColor.appPurpleColor()
        ]
        
        return NSMutableAttributedString(string: DistanceUnit.labels[row], attributes: titleAttributes)
    }
    
}
