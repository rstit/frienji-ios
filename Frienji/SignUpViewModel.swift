//
//  SignUpViewModel.swift
//  Frienji
//
//  Created by bolek on 22.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire

class SignUpViewModel
{
    var username:String!
    var whoAreYou:String!
    
    private let kMinFieldLenght = 3
    
    private let disposeBag = DisposeBag()
    
    //MARK: - Public methods
    
    func checkUsernameAvailability(controller:UIViewController, comletion:(success:Bool, available:Bool)->Void)
    {
        let activityView = ActivityViewController.getActivityInFullScreen("☎️", inView: controller.view)
        
        FrienjiApi.sharedInstance.checkUserNameAvailability(username.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))
            .subscribe(
                onNext: {
                    activityView.hide()
                    comletion(success: true, available: $0)
                },
                onError: { error in
                    activityView.hide()
                    comletion(success: false, available: false)
                    UIAlertUtils.showAlertWithTitle(FrienjiApiError.fromError(error).message, fromController: controller, showCompletion: nil)
                },
                onCompleted: nil,
                onDisposed: nil
            )
            .addDisposableTo(disposeBag)
        
    }
    
    func storeUserInfo()
    {
        Settings.sharedInstance.username = self.username.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        Settings.sharedInstance.whoAreYou = self.whoAreYou.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
    
    func areFieldsValid() -> (Bool, Bool) {
        return (self.username.characters.count >= self.kMinFieldLenght, self.whoAreYou.characters.count >= self.kMinFieldLenght)
    }
}
