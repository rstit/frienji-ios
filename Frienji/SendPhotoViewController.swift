//
//  SendPhotoViewController.swift
//  Frienji
//
//  Created by adam kolodziej on 07.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

protocol SendPhotoViewControllerDelegate: class {
    func sendPhotoController(controller: SendPhotoViewController, didFinishPreparingPhoto photo: UIImage, withMessage message: String)
}

@IBDesignable class SendPhotoViewController: BaseViewController, StoryboardLoad {
    
    @IBOutlet private weak var textInputBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var textInputView: TextInputView!
    @IBOutlet private weak var imageView: UIImageView!
    
    static var storyboardId: String = "Main"
    static var storyboardControllerId = "SendPhotoViewController"
    
    weak var delegate: SendPhotoViewControllerDelegate?
    var photo: UIImage? {
        didSet {
            self.imageView?.image = photo
        }
    }
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textInputView.delegate = self
        
        if photo != nil {
            self.imageView.image = photo
        }
    }
    
    // MARK: - Keyboard Handling
    
    override func keyboardWillChangeSize(newSize size: CGSize) {
        self.textInputBottomConstraint.constant = size.height
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func keyboardWillHide() {
        self.textInputBottomConstraint.constant = 0
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }

}

extension SendPhotoViewController: TextInputProtocol {
    func textInput(textInput: TextInputView, sendPressedWithMessage message: String) {
        guard let photo = photo else {
            assert(false, "There should be a photo provided to view controller")
            return
        }
        self.delegate?.sendPhotoController(self, didFinishPreparingPhoto: photo, withMessage: message)
    }
    
}
