//
//  AvatarSelectionCollectionViewHeader.swift
//  Frienji
//
//  Created by Adam Szeremeta on 29.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class AvatarSelectionCollectionViewHeader: UICollectionReusableView, CellReuseIdentifier {
    
    @IBOutlet weak var titleLabel: UILabel!
    
}
