//
//  Avatar.swift
//  Frienji
//
//  Created by bolek on 17.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics
import Argo
import Curry
import Runes
import Fakery
import SDWebImage

enum AvatarType: Int {
    
    case Emoji
    case Image
    case AnimatedImage
    
    static let allValues: [AvatarType] = [Emoji, Image, AnimatedImage]
    static let labels: [AvatarType: String] = [
        AnimatedImage: Localizations.avatar_selection.familiars,
        Image: Localizations.avatar_selection.weird_creatures,
        Emoji: Localizations.avatar_selection.emoji
    ]
}

struct Avatar {

    let name: String
    let skinTone: String
    let type: AvatarType

    private let cacheService = SDImageCache.sharedImageCache()

    var avatarImage: UIImage {
        if let image = cacheService.imageFromMemoryCacheForKey(name) {
            return image
        }
        let image = createImage() ?? UIImage()
        cacheService.storeImage(image, forKey: name)

        return image
    }

    private func createImage() -> UIImage? {
        switch type {
        case AvatarType.Emoji:
            return UIImage.getEmojiAsImage(name, withSkinTone: skinTone)
        case AvatarType.Image:
            return UIImage(named: name)
        case AvatarType.AnimatedImage:
            return UIImage.getAnimatedImageFrom(name)
        }
    }

}

// MARK: Fake

extension Avatar: Fakeable {

    static func fake() -> Avatar {
        return Avatar(
            name: String.randomEmoji().emojiEscaped,
            skinTone: "",
            type: .Emoji
        )
    }

}

// MARK: Decode

extension Avatar: Decodable {

    static func decode(json: JSON) -> Decoded<Avatar> {
        return curry(self.init)
            <^> json <| "avatar_name"
            <*> json <| "skin_tone"
            <*> json <| "avatar_type"
    }

}

extension AvatarType: Decodable {

    static func decode(json: JSON) -> Decoded<AvatarType> {
        switch json {
        case let .Number(rawValue):
            guard let result = AvatarType(rawValue: rawValue.integerValue) else {
                return .typeMismatch("AvatarType", actual: json)
            }
            return pure(result)
        default:
            return .typeMismatch("Number", actual: json)
        }
    }

}
