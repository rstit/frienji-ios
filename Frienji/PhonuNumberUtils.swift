//
//  PhonuNumberUtils.swift
//  Frienji
//
//  Created by bolek on 23.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import libPhoneNumber_iOS

class PhoneNumberUtils
{
    static let kUkPhoneCoutryCode = 44
    static let kUkPhoneNationaNumberLength = 10
    static let kUkPhoneNationaNumberPrefix = "7"
    
    class func validatePhonuNumber(phoneNumber:String, countryCode:String, countryPhoneCode:Int, completion:(isValid:Bool, formatedPhoneNumber:String?)->Void)
    {
        var isValid = true
        
        do
        {
            let phoneNumber = try NBPhoneNumberUtil.sharedInstance().parse(phoneNumber.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()), defaultRegion: String(countryCode))
            
            isValid = NBPhoneNumberUtil.sharedInstance().isValidNumber(phoneNumber)
            
            if(countryPhoneCode == PhoneNumberUtils.kUkPhoneCoutryCode)
            {
                isValid = String(phoneNumber.nationalNumber).characters.count == PhoneNumberUtils.kUkPhoneNationaNumberLength && String(phoneNumber.nationalNumber).hasPrefix(PhoneNumberUtils.kUkPhoneNationaNumberPrefix)
            }
            
            var formatedPhoneNumber : String?
            
            if(isValid)
            {
                formatedPhoneNumber = try NBPhoneNumberUtil.sharedInstance().format(phoneNumber, numberFormat: NBEPhoneNumberFormat.E164)
            }
            completion(isValid: isValid, formatedPhoneNumber: formatedPhoneNumber)
        }
        catch
        {
            completion(isValid: false, formatedPhoneNumber: nil)
        }
    }
}