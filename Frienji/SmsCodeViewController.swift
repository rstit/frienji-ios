//
//  SmsCodeViewController.swift
//  Frienji
//
//  Created by bolek on 04.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum SmsCodeLenght:Int
{
    case ZeroCharacter = 0
    case OneCharacter = 1
    case TwoCharacters = 2
    case ThreeCharacters = 3
    case FourCharacters = 4
}

class SmsCodeViewController: BaseViewController, StoryboardLoad, UITextFieldDelegate
{
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "SmsCodeViewController"
    
    @IBOutlet var titleLbl:UILabel!
    @IBOutlet var infoLbl:UILabel!
    @IBOutlet var resendCodeBtn:UIButton!
    @IBOutlet var codeBoxView:UIView!
    @IBOutlet var codeTxt:UITextField!
    @IBOutlet var dashLbls: [UILabel]! {
        didSet {
            dashLbls.sortInPlace { $0.0.center.x < $0.1.center.x }
        }
    }
    
    private var viewModel:SmsCodeViewModel!
    private let disposeBag = DisposeBag()
    
    //MARK: - View lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewModel = SmsCodeViewModel()
    }
    
    //MARK: - View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.initControls()
        self.setupCodeInputAccessoryView()
        self.configBindings()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.showKeyboard()
        self.codeTxt.typingAttributes = [NSKernAttributeName : 1.5]
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.dismissKeyboard()
    }
    
    //MARK: - Private methods
    
    private func initControls() {
        self.titleLbl.text = Localizations.smsCode.titleLbl
        self.infoLbl.text = Localizations.smsCode.infoLbl
        
        let resendText = "\(Localizations.smsCode.resend_button_1) \(Localizations.smsCode.resend_button_2)"
            .attributedString().textColor(UIColor.appOrangePlaceholderColor())
            .font(UIFont.latoRegulatWithSize(17.0), forText: Localizations.smsCode.resend_button_1)
        .font(UIFont.latoBoldWithSize(17.0), forText: Localizations.smsCode.resend_button_2)
        self.resendCodeBtn.setAttributedTitle(resendText, forState: UIControlState.Normal)
        self.resendCodeBtn.titleLabel?.textAlignment = NSTextAlignment.Center
    }
    
    private func setupCodeInputAccessoryView() {
        let keyboardInputAccessoryView = UIButton()
        keyboardInputAccessoryView.frame = CGRectMake(0, 0, self.view.frame.size.width, kKeyboardAccesoryButtonHeight)
        keyboardInputAccessoryView.setTitle(Localizations.smsCode.doneBtn, forState: .Normal)
        keyboardInputAccessoryView.backgroundColor = UIColor.appLightBlueColor()
        keyboardInputAccessoryView.titleLabel?.font = UIFont.latoRegulatWithSize(17)
        keyboardInputAccessoryView.addTarget(self, action: #selector(doneClicked), forControlEvents: .TouchUpInside)
        
        self.codeTxt.inputAccessoryView = keyboardInputAccessoryView
    }
    
    private func configBindings()
    {
        self.codeTxt.rx_text.asObservable().bindNext { [unowned self](newCode) in
            self.viewModel.code = newCode
        }.addDisposableTo(self.disposeBag)
        
        let codeDriver = self.codeTxt.rx_text.asDriver()
        for (index, dashLbl) in dashLbls.enumerate() {
            codeDriver.map { code in code.characters.count > index }
                .drive(dashLbl.rx_hidden)
                .addDisposableTo(disposeBag)
        }
    }
    
    private func showKeyboard()
    {
        self.codeTxt.becomeFirstResponder()
    }
    
    private func validateSmsCode()
    {
        self.viewModel.validateSmsCode(self) { (success) in
            if(success)
            {
                let arController = ArViewController.loadFromStoryboard()
                self.navigationController?.pushViewController(arController, animated: true)
                self.navigationController?.viewControllers = [arController]
                self.navigationController?.delegate = TransitionDelegate.sharedInstance
            }
        }
    }
    
    //MARK: - IBAction methods
    
    @IBAction func resendCodeClicked(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func doneClicked(sender:UIButton)
    {
        self.viewModel.isCodeValid { (isValid) in
            if(!isValid)
            {
                UIView.shakeViewHorizontally(self.codeBoxView, times: 4, distance: 8)
                return
            }
            
            self.validateSmsCode()
        }
    }
    
    //MARK: - UITextFieldDelegate
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        guard
            let currentText = textField.text
            else {return true}
        
        let newLength = currentText.characters.count + string.characters.count - range.length
        if(newLength > self.viewModel.kCodeLenght)
        {
            return false
        }
        
        let nonDigitCharacters = NSCharacterSet.decimalDigitCharacterSet().invertedSet
        if (textField.text?.rangeOfCharacterFromSet(nonDigitCharacters) != nil)
        {
            return false
        }
        
        return true
    }
}
