//
//  UIImage+Resize.swift
//  Traces
//
//  Created by Adam Szeremeta on 31.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension UIImage {

    func resizeWithWidth(width: CGFloat) -> UIImage {

        guard self.size.width >= width && self.size.height >= width else {

            return self
        }

        let scale = self.size.width < self.size.height ? width / self.size.width : width / self.size.height
        let newSize = CGSizeMake(self.size.width * scale, self.size.height * scale)

        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.mainScreen().scale)
        self.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return scaledImage ?? self
    }

    func scaleToWidth(width: CGFloat) -> UIImage {
        let newSize = CGSizeMake(width, width)

        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.mainScreen().scale)
    
        let scaledRect = AVMakeRectWithAspectRatioInsideRect(self.size, CGRectMake(0, 0, newSize.width, newSize.height))
        self.drawInRect(scaledRect)

        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return scaledImage ?? self
    }
    
}
