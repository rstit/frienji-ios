//
//  ChatViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 21/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

enum ChatData {
    
    case UserMessage(Message)
    case UserAvatarChange(AvatarChange)
    
    func getId() -> Int {
        switch self {
            
        case .UserMessage(let message):
            return message.id
            
        case .UserAvatarChange(let avatarChange):
            return avatarChange.id
        }
    }
    
    func getDate() -> NSDate {
        switch self {
        
        case .UserMessage(let message):
            return message.createdAt
            
        case .UserAvatarChange(let avatarChange):
            return avatarChange.createdAt
        }
    }
}

class ChatViewModel {
    
    let conversation = Variable<Conversation?>(nil)
    let messages = Variable<[NSDate: [ChatData]]>([NSDate: [ChatData]]())
    let loadMoreTrigger = PublishSubject<Void>()

    let incommingMessage = Variable<Message?>(nil)
    let deletedMessage = Variable<Message?>(nil)
    
    let curentUser: Frienji? = Settings.sharedInstance.userFrienji

    let isInitialFetchInProgress = Variable<Bool>(false)
    let isFetchingNextPage = Variable<Bool>(false)
    let sendingInProgress = Variable<Bool>(false)
    let shouldScrollToBottom = Variable<Bool>(true)
    
    let selectionModeEnabled = Variable<Bool>(false)
    private (set) var selectedMessages = Set<Message>()
    
    let disposeBag = DisposeBag()
    
    private let kDefaultCountperPage = 20
    
    // MARK: Init
    
    init() {
        setUpObservables()
    }
    
    // MARK: Data
    
    func getSectionItemForIndex(index:Int) -> NSDate? {
        return self.messages.value.keys.sort({ (date1:NSDate, date2:NSDate) -> Bool in return date1.compare(date2) == NSComparisonResult.OrderedAscending})[index]
    }
    
    func getSectionDisplayDateForIndex(index:Int) -> NSDate? {
        let key = self.messages.value.keys.sort({ (date1:NSDate, date2:NSDate) -> Bool in return date1.compare(date2) == NSComparisonResult.OrderedAscending})[index]
        
        if let firstMessageData = self.messages.value[key]?.first {
            return firstMessageData.getDate()
        }
        
        return nil
    }
    
    func getRowItemForIndexPath(indexPath:NSIndexPath) -> ChatData? {
        
        guard let date = self.getSectionItemForIndex(indexPath.section) else {
            return nil
        }
        
        return self.messages.value[date]?[indexPath.row]
    }
    
    func getUserForMessage(message:Message?) -> Frienji? {
        return message?.isAuthor ?? false ? self.curentUser : self.conversation.value?.receiver
    }
    
    private func processConversationData(data:ConversationData) {
        let allItems = (data.messages.map { ChatData.UserMessage($0) } + data.avatarChanges.map { ChatData.UserAvatarChange($0) }).sort({ (item1:ChatData, item2:ChatData) -> Bool in
            return item1.getDate().compare(item2.getDate()) == NSComparisonResult.OrderedAscending
        })
        let currentItems = Array(self.messages.value.values.flatMap { $0 })
        
        //splt into sections
        self.splitChatDataIntoSections(currentItems + allItems)
    }
    
    private func splitChatDataIntoSections(data:[ChatData]) {
        var messagesData = [NSDate: [ChatData]]()
        
        //split into sections
        for item in data {
            guard let dateWithoutTime = item.getDate().withoutTime() else {
                continue
            }

            //add to section
            if var sectionData = messagesData[dateWithoutTime] where !sectionData.contains({ (data:ChatData) -> Bool in
                if case .UserMessage(let message) = data {
                    return item.getId() == message.id
                }
                
                if case .UserAvatarChange(let change) = data {
                    return item.getId() == change.id
                }
            
                return false
            }) {
                sectionData.append(item)
                messagesData[dateWithoutTime] = sectionData.sort({ (item1:ChatData, item2:ChatData) -> Bool in
                    return item1.getDate().compare(item2.getDate()) == NSComparisonResult.OrderedAscending
                })
                
            } else {
                messagesData[dateWithoutTime] = [item]
            }
        }
        
        self.messages.value = messagesData
    }
    
    // MARK: Selection
    
    func selectMessageAtIndexPath(indexPath:NSIndexPath) {
        if let rowData = self.getRowItemForIndexPath(indexPath), case .UserMessage(let message) = rowData {
            self.selectedMessages.insert(message)
        }
    }
    
    func deselectMessageAtIndexPath(indexPath:NSIndexPath) {
        if let rowData = self.getRowItemForIndexPath(indexPath), case .UserMessage(let message) = rowData {
            self.selectedMessages.remove(message)
        }
    }
    
    func deselectAllMessages() {
        self.selectedMessages.removeAll()
    }
    
    // MARK: Actions
    
    private func addIncommingMessage(message:Message) {
        let currentMessages = Array(self.messages.value.values.flatMap { $0 })
        let messages = currentMessages + [ChatData.UserMessage(message)]
        
        self.splitChatDataIntoSections(messages)
    }
    
    private func deleteIncommingMessage(deletedMessage:Message) {
        let currentMessages = Array(self.messages.value.values.flatMap { $0 })
        let messages = currentMessages.filter {
            if case .UserMessage(let message) = $0 {
                return deletedMessage.id != message.id
            }
            
            return true
        }

        self.splitChatDataIntoSections(messages)
    }
    
    func postMessageWithArguments(arguments: (String, UIImage?, CLLocationCoordinate2D?), fromController controller: UIViewController) {
        
        guard let conversation = self.conversation.value else {
            return
        }
        
        self.createNewMessageForConversation(conversation, withArguments: arguments, controller: controller)
    }
    
    func deleteSelectedMessages(controller: UIViewController) {
        //remove in API
        self.removeSelectedMessages(controller)
    }
    
    // MARK: Observables
    
    private func setUpObservables() {
        self.conversation.asObservable().bindNext { [unowned self] (conversation:Conversation?) in
            
            guard let conversation = conversation else {
                return
            }
            
            self.fetchMessagesForConversation(conversation)
            
        }.addDisposableTo(self.disposeBag)
     
        self.incommingMessage.asObservable().bindNext { [unowned self] (incomingMessage:Message?) in
            if let incomingMessage = incomingMessage
                where !Array(self.messages.value.values.flatMap { $0 }).contains({ (data:ChatData) -> Bool in
                    if case .UserMessage(let message) = data {
                        return incomingMessage.id == message.id
                    }
                    
                    return false
            }) {
                self.addIncommingMessage(incomingMessage)
            }
        }.addDisposableTo(self.disposeBag)
        
        self.deletedMessage.asObservable().bindNext { [unowned self] (message:Message?) in
            if let message = message {
                self.deleteIncommingMessage(message)
            }
        }.addDisposableTo(self.disposeBag)
    }
    
    // MARK: Api
    
    private func fetchMessagesForConversation(conversation:Conversation) {
        self.isInitialFetchInProgress.value = true
        
        FrienjiApi.sharedInstance.getConversationMessages(conversation, loadMore: self.loadMoreTrigger).doOnError({ [weak self] _ in
            self?.isInitialFetchInProgress.value = false
        }).subscribeNext { [weak self] (data:ConversationData) in
            //process messages
            if let fetching = self?.isFetchingNextPage.value where fetching {
                self?.shouldScrollToBottom.value = false
            }
            
            self?.processConversationData(data)
            
            self?.isInitialFetchInProgress.value = false
            
        }.addDisposableTo(self.disposeBag)
    }
    
    private func createNewMessageForConversation(conversation:Conversation, withArguments arguments: (String, UIImage?, CLLocationCoordinate2D?), controller: UIViewController) {
        self.sendingInProgress.value = true
        
        FrienjiApi.sharedInstance.sendMessage(conversation, content: arguments.0, image: arguments.1, latitude: arguments.2?.latitude, longitude: arguments.2?.longitude)
            .showAlertOnApiError(controller).asObservable().bindNext { [weak self] (message:Message) in
                self?.sendingInProgress.value = false
                self?.shouldScrollToBottom.value = true

                self?.addIncommingMessage(message)
        }.addDisposableTo(self.disposeBag)
    }
    
    private func removeSelectedMessages(controller: UIViewController) {
        
        guard let conversation = self.conversation.value else {
            return
        }
        
        FrienjiApi.sharedInstance.deleteMessages(conversation, messages: Array(self.selectedMessages)).showAlertOnApiError(controller).subscribeNext({ [weak self] in
            
            guard let strongSelf = self else {
                return
            }
            
            let currentItems = Array(strongSelf.messages.value.values.flatMap { $0 })
            let itemsAfterDeleting = currentItems.filter { (data:ChatData) -> Bool in
                if case .UserMessage(let message) = data {
                    return !strongSelf.selectedMessages.contains(message)
                }
                
                return true
            }
            
            strongSelf.splitChatDataIntoSections(itemsAfterDeleting)
            strongSelf.deselectAllMessages()
        }).addDisposableTo(self.disposeBag)
    }
    
}
