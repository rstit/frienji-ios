//
//  Introduction1ViewController.swift
//  Frienji
//
//  Created by bolek on 29.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

let kKeyboardAccesoryButtonHeight: CGFloat = 60

class IntroductionViewController: UIViewController, StoryboardLoad {
    
    static let storyboardId: String = "Introductions"
    static let storyboardControllerId = "IntroductionViewController"
    
    // MARK: Life cycle
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //MARK: - IBActions
    
    @IBAction func nextBtnClicked(sender:AnyObject) {
        if(!PermissionsHelper.hasGrantedCameraPermission()) {
            self.performSegueWithIdentifier("cameraPermissionSegue", sender: nil)
        } else if(!PermissionsHelper.isRegisterdForPushNotifications()) {
            self.performSegueWithIdentifier("pushPermissionSegue", sender: nil)
        } else if(!PermissionsHelper.hasGrantedLocationPermission()) {
            self.performSegueWithIdentifier("locationPermissionSegue", sender: nil)
        } else if(Settings.sharedInstance.userVerified) {
            self.performSegueWithIdentifier("arSegue", sender: nil)
        } else {
            self.performSegueWithIdentifier("signInOrUpSegue", sender: nil)
        }
    }
    
    
}
