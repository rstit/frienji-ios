//
//  TextInputView.swift
//  Frienji
//
//  Created by adam kolodziej on 06.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

protocol TextInputProtocol: class {
    func textInput(textInput: TextInputView, sendPressedWithMessage message: String)
}

@IBDesignable class TextInputView: UIView, NibInstantiate {

    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var sendButton: UIButton!
    
    weak var delegate: TextInputProtocol?
    
    var sendEnabled: Bool {
        get {
            return sendButton.enabled
        }
        set {
            sendButton.enabled = newValue
            sendButton.alpha = newValue ? 1 : 0.5
        }
    }
    
    // MARK: - Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    private func loadViewFromNib() {
        let view = self.instantiateFromNib()
        self.addSubviewFullscreen(view)
    }
    
    // MARK: - Actions

    @IBAction private func sendButtonPressed(sender: AnyObject) {
        self.delegate?.textInput(self, sendPressedWithMessage: textView.text)
    }
}
