//
//  UIView+Shaker.swift
//  Frienji
//
//  Created by bolek on 03.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

extension UIView
{
    class func shakeViewHorizontally(view:UIView, times:Int, distance:CGFloat)
    {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-distance,distance,-distance,distance,-distance,distance,-distance,distance,0]
        view.layer.addAnimation(animation, forKey: "shake")
    }
}
