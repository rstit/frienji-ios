//
//  ArViewController.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift
import RxCocoa
import CoreLocation
import CoreMotion
import SDWebImage

class ArViewController: UIViewController, StoryboardLoad {

    private let kUserAvatarButtonSize = CGSizeMake(80, 80)

    static var storyboardId: String = "Main"

    @IBOutlet weak var arOpenGLView: ArOpenGLView!
    @IBOutlet weak var profileButton: UIButton!

    // MARK: - Properties
    
    private var cameraSession: ArCameraSession?
    private let disposeBag = DisposeBag()

    private var wasPermissionDeniedAlertShown = false

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        registerForAppLifecycleEvents()

        #if !(arch(i386) || arch(x86_64))
            self.cameraSession = ArCameraSession()
        #endif
        self.cameraSession?.delegate = self.arOpenGLView

        self.arOpenGLView.cameraAngle = self.cameraSession?.getCameraFieldOfView() ?? 0

        setUpBindings()
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.activateRenderer()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        self.showPermissionsDeniedAlert()
        self.showOpenGLAlert()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        deactivateRenderer()
    }

    private func showPermissionsDeniedAlert() {

        guard !self.wasPermissionDeniedAlertShown else {
            return
        }

        let isCameraDenied = ArCameraSession.getCameraPermission() == .Denied
        let isLocationDenied = LocationManager.getLocationPermission() == .Denied

        var message = ""

        if isCameraDenied && isLocationDenied {
            message = "\(Localizations.alert.camera) \(Localizations.alert.and) \(Localizations.alert.location) \(Localizations.alert.permissions_denied)"

        } else if isCameraDenied {
            message = "\(Localizations.alert.camera) \(Localizations.alert.permissions_denied)"

        } else if isLocationDenied {
            message = "\(Localizations.alert.location) \(Localizations.alert.permissions_denied)"
        }

        if !message.isEmpty {
            UIAlertUtils.showAlertWithTitle(message, fromController: self, showCompletion: nil)
        }
    }

    private func showOpenGLAlert() {
        if !self.arOpenGLView.isOpenGLProgramCorrect() {
            UIAlertUtils.showAlertWithTitle(Localizations.ar.opengl_invalid_state, fromController: self, showCompletion: nil)
        }
    }

    // MARK: Notifications

    private func registerForAppLifecycleEvents() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ArViewController.appWillEnterForeground), name: UIApplicationWillEnterForegroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ArViewController.appWillEnterForeground), name: UIApplicationDidBecomeActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ArViewController.appWillEnterBackground), name: UIApplicationWillResignActiveNotification, object: nil)
    }

    func appWillEnterForeground() {
        dispatch_async(dispatch_get_main_queue()) {
            self.activateRenderer()
        }
    }

    func appWillEnterBackground() {
        dispatch_async(dispatch_get_main_queue()) {

            self.deactivateRenderer()
        }
    }

    // MARK: Bindings

    private func setUpBindings() {
        // we want to ask for permissions one after another
        self.cameraSession?.permissionAlertShown.asObservable().skip(1).bindNext { (result: Bool) in
            LocationManager.sharedInstance.requestWhenInUseAuthorization()
        }.addDisposableTo(self.disposeBag)

        LocationManager.sharedInstance.currentLocation.asObservable().bindNext { [weak self] (userLocation: CLLocation?) in
            self?.arOpenGLView.updateUserLocation(userLocation)
        }.addDisposableTo(self.disposeBag)

        LocationManager.sharedInstance.currentHeading.asObservable().bindNext { [weak self] (heading: Double?) in
            self?.arOpenGLView.updateHeading(heading)
        }.addDisposableTo(self.disposeBag)

        MotionManager.sharedInstance.currentInclination.asObservable().bindNext { [weak self] (inclination: CGFloat) in
            self?.arOpenGLView.updateInclination(inclination)
        }.addDisposableTo(self.disposeBag)
    }

    // MARK: Render

    func activateRenderer() {
        self.cameraSession?.startCameraSession()
        self.arOpenGLView?.activateRenderer()
        self.arOpenGLView?.updateUserLocation(LocationManager.sharedInstance.currentLocation.value)

        LocationManager.sharedInstance.startLocationUpdates()
        MotionManager.sharedInstance.startMotionUpdates()
    }

    func deactivateRenderer() {
        self.cameraSession?.stopCameraSession()
        self.arOpenGLView?.deactivateRenderer()

        LocationManager.sharedInstance.stopLocationUpdates()
        MotionManager.sharedInstance.stopMotionUpdates()
    }

    // MARK: Profile button

    func showProfileButton() {
        UIView.animateWithDuration(kDefaultAnimationDuration) {

            self.profileButton?.alpha = 1
            self.arOpenGLView?.radarView?.alpha = 1
        }
    }

    func hideProfileButton() {
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.profileButton?.alpha = 0
            self.arOpenGLView?.radarView?.alpha = 0
        }
    }

    // MARK: Animation

    func animateProfileIconAfterCatchingTrace() {
        CATransaction.begin()

        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 1.5
        animation.values = [0.5, 1.5, 0.65, 1.35, 0.75, 1.25, 0.85, 1.15, 1]

        CATransaction.setCompletionBlock {
            self.profileButton.layer.removeAllAnimations()
        }

        self.profileButton.layer.addAnimation(animation, forKey: "scale")

        CATransaction.commit()
    }

}
