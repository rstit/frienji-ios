//
//  SettingsViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 27/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import Curry

class SettingsViewModel {
    
    enum FieldsValidation {
        
        case Name
        case Bio
        
        static let validationRules = [
            Name: (SettingsViewModel.kNameValidationMaxCharactersCount, Localizations.settings.name_too_long),
            Bio: (SettingsViewModel.kBioValidationMaxCharactersCount, Localizations.settings.bio_too_long)
        ]
    }

    static let kNameValidationMaxCharactersCount = 20
    static let kBioValidationMaxCharactersCount = 140

    // MARK: - Outputs
    
    let saveInProgress = Variable<Bool>(false)

    let name = Variable<String>("")
    let bio = Variable<String>("")
    let avatar = Variable<UIImage?>(nil)
    let cover = Variable<UIImage?>(nil)
    let distanceUnit = Variable<DistanceUnit>(Settings.sharedInstance.distanceUnit)

    let nameIsValid = Variable<Bool>(true)
    let bioIsValid = Variable<Bool>(true)
    let isValid: Observable<Bool>
    let error = PublishSubject<Void>()

    private (set) var changesPresent = false
    
    // MARK: - Inputs

    let initialized = PublishSubject<Void>()
    let avatarTapped = PublishSubject<Void>()
    let avatarSelected = BehaviorSubject<Avatar?>(value: nil)
    let saved = PublishSubject<Void>()

    let disposeBag = DisposeBag()

    // MARK: - Dependencies

    private let navigation: NavigationHandler
    private let settings = Settings.sharedInstance
    private let api = FrienjiApi.sharedInstance

    // MARK: - Initialization

    init(navigationHandler: NavigationHandler, attachmentHandler: AttachmentHandler<SettingsViewController>) {
        navigation = navigationHandler
        isValid = Observable.combineLatest(
            nameIsValid.asObservable(),
            bioIsValid.asObservable()
        ) { $0 && $1 }

        prepareLoad()
        prepareSave()
        prepareAvatarSelection()
        prepareCoverSelection(attachmentHandler.image)
        prepareValidation()
        
        Observable.combineLatest(self.name.asObservable(), self.bio.asObservable(), self.avatar.asObservable(), self.cover.asObservable()) { _ -> Bool in
            return true
        }.skip(5).bindNext { [unowned self] (changes:Bool) in //5 - one for each initial binding and one additional ??
            self.changesPresent = changes
        }.addDisposableTo(self.disposeBag)
    }

    func prepareLoad() {
        let profile = initialized
            .flatMap { [unowned self] in
                self.settings.userFrienji.map(Observable.just) ?? .empty()
            }
            .shareReplay(1)

        profile
            .map { $0.username }
            .bindTo(name)
            .addDisposableTo(disposeBag)
        profile
            .flatMap { $0.whoAreYou.map(Observable.just) ?? .empty() }
            .bindTo(bio)
            .addDisposableTo(disposeBag)
        profile
            .map { $0.avatar.avatarImage }
            .bindTo(avatar)
            .addDisposableTo(disposeBag)
        profile
            .map { $0.backgroundImageUrl.flatMap(NSURL.init) }
            .flatMap { [unowned self] url -> Observable<ImageLoad> in
                url?.webImage
                    .doOnError { self.error.onError($0) }
                ?? .empty()
            }
            .map { $0.image ?? UIImage() }
            .bindTo(cover)
            .addDisposableTo(disposeBag)
    }

    func prepareSave() {
        saved
            .doOn { [weak self] event in
                self?.saveInProgress.value = true
            }
            .map { [unowned self] in
                
                return curry(self.api.updateProfile)
            }
            .withLatestFrom(name.asObservable()) { $0($1) }
            .withLatestFrom(bio.asObservable()) { $0($1) }
            .withLatestFrom(avatarSelected) { $0($1) }
            .withLatestFrom(cover.asObservable()) { $0($1) }
            .flatMap { [unowned self] updateProfile -> Observable<Frienji?> in
                return updateProfile
                    .map(Optional.init)
                    .doOnError { self.error.onError($0) }
                    .catchErrorJustReturn(nil)
            }
            .bindTo(navigation.popped(WallViewController.self)) { [weak self] input, user in
                
                if let unit = self?.distanceUnit.value {
                    Settings.sharedInstance.distanceUnit = unit
                }
                
                self?.saveInProgress.value = false
                
                if let user = user {
                    input.loaded.onNext(user)
                }
            }
            .addDisposableTo(disposeBag)
    }

    func prepareAvatarSelection() {
        avatarTapped
            .subscribeNext { [unowned self] in
                self.navigation.presentModal(AvatarSelectionViewController.self)
            }
            .addDisposableTo(disposeBag)
        avatarSelected
            .map { $0?.avatarImage }
            .bindTo(avatar)
            .addDisposableTo(disposeBag)
    }

    func prepareCoverSelection(coverSelected: Observable<UIImage?>) {
        coverSelected
            .bindTo(cover)
            .addDisposableTo(disposeBag)
    }

    func prepareValidation() {
        name.asObservable()
            .map {
                $0.characters.count <= SettingsViewModel.kNameValidationMaxCharactersCount
            }
            .bindTo(nameIsValid)
            .addDisposableTo(disposeBag)
        bio.asObservable()
            .map {
                $0.characters.count <= SettingsViewModel.kBioValidationMaxCharactersCount
            }
            .bindTo(bioIsValid)
            .addDisposableTo(disposeBag)
    }

}
