//
//  ActivitiesViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 26/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ActivitiesViewController: UITableViewController {

    @IBOutlet weak var dismissButton: UIBarButtonItem!

    var viewModel: ActivitiesViewModel!

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = nil
        self.tableView.estimatedRowHeight = ActivityCell.kEstimatedViewHeight
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        viewModel = ActivitiesViewModel(navigationHandler: NavigationHandler(sourceViewController: self))
        createBindings()
        viewModel.loadInitialized.onNext()
        
        navigationController?.navigationBar.backIndicatorImage = UIImage()
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.UserActivities) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.UserActivities) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.UserActivities)
            }
            coachmark.showWithAnimationInContainer()
        }
        
        //reload table to refresh the time
        self.viewModel.triggerDataReload()
    }
    
    func createBindings() {
        // Table view
        viewModel.activities.asObservable()
            .bindTo(tableView.rx_itemsWithCellIdentifier(R.reuseIdentifier.activityCell.identifier)) { (row, activity, cell: ActivityCell) in
                cell.configure(activity)
            }
            .addDisposableTo(disposeBag)
        
        Observable.combineLatest(tableView.rx_modelSelected(Activity<Any>.self).asObservable(), Settings.sharedInstance.rx_userFrienji) { (activity: $0, frienji: $1) }
            .subscribeNext { [weak self] activity, frienji in
                self?.performSegue(for: activity, with: frienji)
        }.addDisposableTo(disposeBag)
        
        // Load more
        tableView.loadMoreTrigger
            .bindTo(viewModel.loadMoreTriggered)
            .addDisposableTo(disposeBag)
        
        viewModel.activities.asObservable().bindNext { [weak self] (activities:[Activity<Any>]) in
            self?.refreshControl?.endRefreshing()
        }.addDisposableTo(self.disposeBag)

        // Navigation
        dismissButton.rx_tap
            .bindTo(viewModel.dismissTapped)
            .addDisposableTo(disposeBag)

        // Errors
        viewModel.apiError
            .showAlertOnApiError(self)
            .catchErrorJustReturn()
            .subscribe()
            .addDisposableTo(disposeBag)
    }

    // MARK: - Table view delegate

    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return .None
    }
    
    // MARK: Pull to refresh
    
    @IBAction func pullToRefreshDidTrigger(sender: AnyObject) {
        self.viewModel.loadInitialized.onNext()
    }
    
    //MARK: - Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if let activity = sender as? Observable<Activity<Any>> {
            Observable.combineLatest(activity, Settings.sharedInstance.rx_userFrienji) { (activity: $0, frienji: $1) }
                .subscribeNext { [weak self] activity, frienji in
                    self?.prepare(for : segue, with: activity, with: frienji)
            }.addDisposableTo(disposeBag)
        }
    }
    
    func performSegue(for activity: Activity<Any>, with frienji: Frienji) {
        let segueIdentifier: String?
        
        switch activity.key {
        case .IncomingMessage:
            segueIdentifier = R.segue.activitiesViewController.frienjiChat.identifier
            
        case .CommentOnMyWall:
            segueIdentifier = R.segue.activitiesViewController.showMyWallsComments.identifier
            
        case .Catch:
            segueIdentifier = R.segue.activitiesViewController.showFrienji.identifier            
            
        case .Post, .LikePost:
            if wallOwnerId(for: activity) == frienji.identity {
                segueIdentifier = R.segue.activitiesViewController.showMyWall.identifier
            } else {
                segueIdentifier = R.segue.activitiesViewController.showFrienji.identifier
            }
            
        case .Comment, .LikeComment:
            if wallOwnerId(for: activity) == frienji.identity {
                segueIdentifier = R.segue.activitiesViewController.showMyWallsComments.identifier
            } else {
                segueIdentifier = R.segue.activitiesViewController.showComments.identifier
            }
            
        default:
            return
        }
        
        if let segueIdentifier = segueIdentifier {
            self.performSegueWithIdentifier(segueIdentifier, sender: Observable.just(activity))
        }
    }
    
    func prepare(for segue: UIStoryboardSegue, with activity: Activity<Any>, with myFrienji: Frienji) {
        if let destination = segue.destinationViewController as? ChatViewController {
            FrienjiApi.sharedInstance.createConversation(activity.owner).showAlertOnApiError(self).catchError { _ in
                Observable.empty()
                }.asObservable().subscribeNext({ conversation in
                    destination.setConversation(conversation)
                }).addDisposableTo(disposeBag)
            return
        } else if let wallViewController = segue.destinationViewController as? WallViewController {
            prepare(wallViewController, for: segue, with: activity, with: myFrienji)
        }
    }
    
    func prepare(wallViewController: WallViewController, for segue: UIStoryboardSegue, with activity: Activity<Any>, with myFrienji: Frienji) {
        guard let segueIdentifier = segue.identifier else {
            return
        }
        
        switch segueIdentifier {
        case R.segue.activitiesViewController.showMyWall.identifier:
            wallViewController.input.loaded.onNext(myFrienji)
        case R.segue.activitiesViewController.showMyWallsComments.identifier:
            wallViewController.input.loaded.onNext(myFrienji)
            if let wallCommentsViewController = segue.destinationViewController as? WallCommentsViewController {
                loadPost(to: wallCommentsViewController, for: activity)
            }
        case R.segue.activitiesViewController.showFrienji.identifier:
            loadFrienji(to: wallViewController, for: activity)
            
        case R.segue.activitiesViewController.showComments.identifier:
            loadFrienji(to: wallViewController, for: activity)
            if let wallCommentsViewController = segue.destinationViewController as? WallCommentsViewController {
                loadPost(to: wallCommentsViewController, for: activity)
            }
        default:
            return
        }
    }
    
    // MARK: Helpers
    
    func loadFrienji(to wallViewController: WallViewController, for activity: Activity<Any>) {
        guard let wallOwnerId = wallOwnerId(for: activity) else {
            assert(false, "Unhandled type of entity")
            return
        }
        FrienjiApi.sharedInstance.getFrienji(wallOwnerId) .showAlertOnApiError(self).catchError { _ in
            Observable.empty()
            }.asObservable().subscribeNext({ frienji in
                wallViewController.input.loaded.onNext(frienji)
            }).addDisposableTo(self.disposeBag)
    }
    
    func loadPost(to wallCommentsViewController: WallCommentsViewController, for activity: Activity<Any>) {
        guard let wallOwnerId = wallOwnerId(for: activity) else {
            assert(false, "Unhandled type of entity")
            return
        }
        guard let postId = postId(for: activity) else {
            assert(false, "Unhandled type of entity")
            return
        }
        FrienjiApi.sharedInstance.getPost(wallOwnerId, postId: postId) .showAlertOnApiError(self).catchError { _ in
            Observable.empty()
            }.asObservable().subscribeNext({ post in
                wallCommentsViewController.postLoaded.onNext(post)
            }).addDisposableTo(self.disposeBag)
    }
    
    func postId(for activity: Activity<Any>) -> Int? {
        if let post = activity.entity as? Post {
            return post.parentPostId
        } else if let like = activity.entity as? Like {
            return like.parentPostId
        } else {
            return nil
        }
    }
    
    func wallOwnerId(for activity: Activity<Any>) -> Int? {
        if let post = activity.entity as? Post {
            return post.wallOwnerId
        } else if let like = activity.entity as? Like {
            return like.wallOwnerId
        } else if activity.key == .Catch {
            return Int(activity.owner.dbID)
        } else {
            return nil
        }
    }

}
