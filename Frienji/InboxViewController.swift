//
//  InboxViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 18.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SegueKit

struct InboxInput {
    let newConversationStarted = PublishSubject<Conversation>()
}

class InboxViewController: UIViewController, StoryboardLoad, HasInput {

    static var storyboardId: String = "Main"
    static var storyboardControllerId = "InboxViewController"

    //outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    //properties
    let input = InboxInput()
    let disposeBag = DisposeBag()

    // MARK: - View's life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Initial load
        let loadCommand = FrienjiApi.sharedInstance.getConversations(tableView.loadMoreTrigger)
            .showAlertOnApiError(self)
            .asDriver(onErrorJustReturn: [])
            .map(InboxCommand.Load)
            .asObservable()

        // Add new conversation
        let addCommand = input.newConversationStarted.map(InboxCommand.Add)

        // View model
        let viewModel = Observable.of(loadCommand, addCommand)
            .merge()
            .scan(InboxViewModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .shareReplay(1)

        // Bind view model
        viewModel
            .map { $0.conversations }
            .bindTo(tableView.rx_itemsWithCellIdentifier(InboxTableViewCell.identifier)) { (row, conversation, cell: InboxTableViewCell) in
                cell.content.onNext(conversation)
            }
            .addDisposableTo(disposeBag)

        viewModel.bindNext { [weak self] _ in
            self?.loadingIndicator.hidden = true
        }.addDisposableTo(self.disposeBag)
        
        viewModel.map { $0.conversations.count != 0 }.bindTo(self.emptyLabel.rx_hidden).addDisposableTo(self.disposeBag)
        viewModel.map { $0.conversations.count == 0 }.bindTo(self.tableView.rx_hidden).addDisposableTo(self.disposeBag)
        
        // Mark as read
        let newConversationIndexPath: Observable<NSIndexPath> = input.newConversationStarted
            .withLatestFrom(viewModel) { conversation, viewModel in
                NSIndexPath(forRow: viewModel.conversations.indexOf(conversation)!, inSection: 0)
            }
        Observable.of(tableView.rx_itemSelected.asObservable(), newConversationIndexPath).merge()
            .map(tableView.cellForRowAtIndexPath)
            .subscribeNext { [unowned self] cell in
                if let cell = cell as? InboxTableViewCell {
                    cell.markedAsRead.onNext()
                }
                
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
                }
            }
            .addDisposableTo(disposeBag)

        // Navigation
        Observable.of(
            tableView.rx_modelSelected(Conversation.self).asObservable(),
            input.newConversationStarted.asObservable()
            )
            .merge()
            .bindTo(rx_segue(R.segue.inboxViewController.inboxChat.identifier)) { segue, conversation in
                if let chatViewController = segue.destinationViewController as? ChatViewController {
                    chatViewController.setConversation(conversation)
                }
            }
            .addDisposableTo(disposeBag)
    }

}
