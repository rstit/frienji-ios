//
//  FromJSON.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

protocol FromJson {
    
    associatedtype T
    
    static func fromJson(json:JSON) -> T?
}