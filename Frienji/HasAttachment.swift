//
//  HasAttachment.swift
//  Frienji
//
//  Created by Piotr Łyczba on 20/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift

protocol HasAttachment {

    var attachmentRequested: Observable<Void> { get }

}
