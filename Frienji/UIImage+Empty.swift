//
//  UIImage+Empty.swift
//  Frienji
//
//  Created by Piotr Łyczba on 02/11/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {

    static func emptyImageWithSize(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }

}
