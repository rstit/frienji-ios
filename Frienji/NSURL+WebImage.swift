//
//  NSURL+Image.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import SDWebImage

enum ImageLoad {
    case InProgress(value: Double)
    case Success(value: UIImage)
    case Failure(error: ErrorType)

    var image: UIImage? {
        guard case let .Success(value) = self else {
            return nil
        }
        return value
    }

}

extension NSURL {

    var webImage: Observable<ImageLoad> {
        return Observable.create { observer in
            SDWebImageManager.sharedManager()
                .downloadImageWithURL(self,
                    options: [],
                    progress: { received, expected in
                        let progress = Double(received) / Double(expected)
                        observer.onNext(.InProgress(value: progress))
                    },
                    completed: { image, error, _, _, _ in
                        if let error = error {
                            observer.onNext(.Failure(error: error))
                        } else if let image = image {
                            observer.onNext(.Success(value: image))
                        }
                    }
                )

            return NopDisposable.instance
        }
    }

}
