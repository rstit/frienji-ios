//
//  BlockedFrienjiViewModel.swift
//  Frienji
//
//  Created by adam kolodziej on 19.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

import Foundation
import SwiftOrderedSet

struct BlockedFrienjiViewModel {
    
    let blockedFrienjis: [Frienji]
    let unblockedFrienjisWithIndices: [(index: Int, frienji: Frienji)]
    
    var unblockedFrienjis: [Frienji] {
        return unblockedFrienjisWithIndices.map { $0.frienji }
    }
    
    init(blockedFrienjis: [Frienji] = [], unblockedFrienjis: [(index: Int, frienji: Frienji)] = []) {
        self.blockedFrienjis = blockedFrienjis
        self.unblockedFrienjisWithIndices = unblockedFrienjis
    }
    
    func executeCommand(command: BlockedFrienjiCommand) -> BlockedFrienjiViewModel {
        switch command {
        case let .LoadBlockedFrienjis(newFrienjis):
            return BlockedFrienjiViewModel(blockedFrienjis: newFrienjis)
        case let .StopBlockingFrienjiAtIndexPath(index):
            let newBlockedFrienjis = blockedFrienjis.arrayByRemovingAtIndex(index)
            let unblockedFirenjis = blockedFrienjis[index]
            
            return BlockedFrienjiViewModel(blockedFrienjis: newBlockedFrienjis, unblockedFrienjis: [(index, unblockedFirenjis)])
        case .CommitUnblock:
            return BlockedFrienjiViewModel(blockedFrienjis: blockedFrienjis)
        }
    }
    
}

enum BlockedFrienjiCommand {
    case LoadBlockedFrienjis(frienjis: [Frienji])
    case StopBlockingFrienjiAtIndexPath(index: Int)
    case CommitUnblock()
}
